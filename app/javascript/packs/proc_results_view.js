import ProcResult from "./views/proc_result";
window.ProcResult = ProcResult;

import Procedure from "./views/procedure";
window.Procedure = Procedure;

import Tag from "./views/tag";
window.Tag = Tag;

import Spr from "./views/spr";
window.Spr = Spr;

import Comments from "./views/comments";
window.Comments = Comments;

import Requirements from "./views/requirements";
window.Requirements = Requirements;