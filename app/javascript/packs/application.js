// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("@rails/activestorage").start()
require('channels')

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

window.$ = require('jquery')

require('popper.js')
require('bootstrap')

require('chart.js')
require('chartjs-plugin-datalabels')

require('datatables.net')
require('datatables.net-bs4')
require("datatables.net-bs4/css/dataTables.bootstrap4.min.css")
require("datatables.net-buttons")
require("datatables.net-buttons/js/buttons.html5")
require("datatables.net-buttons-bs4")

let AutoComplete = require("autocomplete-js");
require("autocomplete-js/dist/autocomplete.min.css")

import {renderTag} from './utils/functions'

$(document).ready(() => {
    AutoComplete();

    $('.datatable').DataTable( {
        lengthMenu: [ [20, 50, 100, -1], [20, 50, 100, "All"] ],
        responsive: true,
        order: [[0, 'desc']],
        columnDefs: [{ targets: 'no-sort', orderable: false }],
        dom: '<"row" <"col-md-6"l><"col-md-6"f>>t<"row"<"col-md-4"B><"col-md-4"i><"col-md-4"p>>',
        buttons: [
            {
                extend: 'csv',
                fieldSeparator: ';',
                className: 'btn-sm',
                text: '<em class="fa fa-download"></em> CSV'
            }
        ]
    } );

    $('[data-toggle="tooltip"]').tooltip();
});
