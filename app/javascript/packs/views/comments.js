'use strict';

import {renderTag, scrollDown} from "../utils/functions";

let $ = require('jquery')
import Flash from "../utils/flash";

export default class Comments {

    constructor(resultId) {
        this.resultId = resultId;
    };

    /**
     * Add a new comment
     * @param formId the HTML id of the form to send
     * @param tableId the HTML id of the table to append the new comment to
     */
    add_comment(formId, tableId) {
        let self = this;
        let form = $(`#${formId}`);
        $.post('/comments', form.serialize(), () => {})
            .done((data) => {
                self.append_comment(tableId, data);
                form.trigger('reset');
            })
            .fail((data) => { Flash.error(data.responseText); });
    }

    /**
     * Removes a content from the given table and adds comments for the current procedure result
     * @param tableId {String} the id of the table HTML element
     */
    render_result_comments(tableId) {
        $.get(`/comments/${this.resultId}`, (data) => {
            document.getElementById(tableId).innerHTML = '';
            data.forEach(comment => { this.append_comment(tableId, comment); });
        });
    }

    /**
     * Removes a content from the given table and adds comments for the procedure corresponding to the current result
     * @param tableId {String} the id of the table HTML element
     */
    render_procedure_comments(tableId) {
        $.get(`/comments/${this.resultId}/all`, (data) =>{
            document.getElementById(tableId).innerHTML = '';
            data.forEach(comment => { this.append_comment(tableId, comment); });
        });
    }

    /**
     * Appends the comment a s row to the given table
     * @param tableId {String} the id of the table HTML element
     * @param comment {JSON} the comment to append
     */
    append_comment(tableId, comment) {
        let innerHTML = document.getElementById(tableId).innerHTML;
        document.getElementById(tableId).innerHTML = innerHTML + this.comment_row(comment);
        scrollDown('scroll-box');
    }

    /**
     * Generates HTML for the given comment
     * @param comment {JSON} the comment in JSON format
     * @return {String} the generated HTML
     */
    comment_row(comment) {
        let html = '<tr>';
        html += `<td class="small">${comment.created_at}</td>`;
        html += `<td class="small">${comment.user}</td>`;
        html += `<td>${comment.text}</td>`;
        html += '</tr>';
        return html;
    }
}