'use strict';

import Chart from 'chart.js'
import $ from 'jquery'

require('datatables.net')
require("datatables.net-buttons")
require("datatables.net-buttons/js/buttons.colVis")
require("datatables.net-buttons/js/buttons.html5")
require("datatables.net-buttons-bs4")

import {renderSummary} from '../utils/functions'

export default class ProcResult {

    constructor(runId) {
        this.runId = runId;
    };

    /**
     * Fetch results as JSON form the backend and render a datatable
     */
    render_history(url) {
        let runId = this.runId;
        $("#resultsHistory").DataTable({
            order: [[5, 'desc']],
            processing: true,
            serverSide: true,
            responsive: true,
            lengthMenu: [20, 40, 60, 80, 100],
            ajax: url,
            columns: [
                {
                    data: 'test_run_id',
                    name: 'test_run_id',
                    title: "Run",
                    className: 'cell-bold',
                    render: (data) => {
                        return `#${data}`
                    }
                },
                {
                    data: "group",
                    title: "Group",
                    name: 'run_groups.name',
                },
                {
                    data: "specification",
                    title: "Specification",
                    name: 'test_runs.specification',
                },
                {
                    data: "environment",
                    title: "Environment",
                    name: 'test_runs.environment',
                },
                {
                    data: "start_time",
                    title: "Started",
                    name: 'procedure_results.start_time'
                },
                {
                    data: "end_time",
                    title: "Finished",
                    name: 'procedure_results.end_time'
                },
                {
                    data: "result",
                    title: "Result",
                    name: 'procedure_results.result',
                    createdCell: (td, _cellData, row) => {
                        $(td).addClass(`cell-${row.result_css}`);
                    }
                },
                {
                    data: null,
                    title: "Summary",
                    name: 'procedure_results.message',
                    render: (row) => {
                        return renderSummary(row.summary)
                    }
                },
                {
                    data: null,
                    title: "",
                    orderable: false,
                    render: (row) => {
                        return `<a href="/results/${row.test_run_id}/${row.name}" title="Detail Results for '${row.name}'"><em class="fa fa-file-medical-alt"></em></a>`
                    }
                }
            ],
            dom: '<"row" <"col-md-6"l><"col-md-6"f>>t<"row"<"col-md-4"B><"col-md-4"i><"col-md-4"p>>',
            buttons: [
                {
                    extend: 'csv',
                    fieldSeparator: ';',
                    className: 'btn-sm',
                    text: '<em class="fa fa-download"></em> CSV'
                }
            ],
            createdRow: function (row, data, _idx) {
                if (data.test_run_id === runId) {
                    $(row).addClass('row-highlight');
                }
            }
        });
    }
}