'use strict';

let $ = require('jquery')
import {renderTag} from '../utils/functions'
import Flash from "../utils/flash";

export default class Tag {

    /**
     * Generates an HTML representation from a tag in JSON
     * @param {String} elementId the element to append the tag to
     * @param {Object} tag the Tag in JSON
     */
    append_tag(elementId, tag) {
        let original = document.getElementById(elementId).innerHTML;
        document.getElementById(elementId).innerHTML= original + renderTag(tag, true);
    }

    /**
     * Generates an HTML representation from a tag list in JSON
     * @param {String} elementId the element to append the tag to
     * @param {Object} tags Array of Tags in JSON
     */
    append_tags(elementId, tags) {
        let innerHTML = document.getElementById(elementId).innerHTML;
        tags.forEach(tag => { innerHTML += renderTag(tag, true); })
        document.getElementById(elementId).innerHTML= innerHTML;
    }

    activate_tag_remove(procId) {
        $('.btn-tag-del').each(function() {
            let elem = $(this);
            let tagId = elem.attr('id').match(/\d+/)[0];
            elem.unbind();
            elem.click(() => {
                $.get(`/tags/unlink?tag=${tagId}&procedure=${procId}`)
                    .done(() => { $(`#tag-${tagId}`).remove(); })
                    .fail((data) => { Flash.error(data.responseText); });
            })
        })
    }

    create_link(formId, targetId) {
        let self = this;
        let formData = $(`#${formId}`)
        $.post(`/tags/create_link`, formData.serialize(), () => {})
            .done((data) => {
                self.append_tag(targetId, data);
                self.activate_tag_remove(formData.find('#object_id').val());
            })
            .fail((data) => { Flash.error(data.responseText); });
    }

    /**
     * Picks the color for the 'color' input field
     * @param {String} color the hex code of the color to pick
     */
    pick(color) {
        document.getElementById('color').value = color;
    }
}