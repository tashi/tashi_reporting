import Chart from "chart.js";
import $ from 'jquery'
import 'datatables.net'

require('datatables.net-bs4')
require("datatables.net-bs4/css/dataTables.bootstrap4.min.css")
require("datatables.net-buttons")
require("datatables.net-buttons/js/buttons.html5")
require("datatables.net-buttons-bs4")

import {renderTag} from "../utils/functions";

export default class RunHistory {

    render_duration(elementId, data) {
        let labels = [];
        data.forEach(run => { labels.push(`#${run.id}`); });

        let set_data = [];
        data.forEach(run => { set_data.push(run.duration); });

        let datasets = [{
                data: set_data,
                label: 'Duration (Minutes)',
                backgroundColor: 'rgba(120, 110, 250, 0.6)',
                borderColor: 'rgba(120, 110, 250, 0.6)',
                fill: false
            }];

        this.render_chart(elementId, labels, datasets);
    }

    render_trend(elementId, data) {
        let states = {
            'Passed': 'rgba(0, 255, 0, 0.45)',
            'Passed with observations': 'rgba(255, 230, 0, 0.6)',
            'Failed': 'rgba(255, 0, 0, 0.6)',
            'Not Executed': 'rgba(150, 150, 150, 0.6)',
            'total': 'rgba(120, 110, 250, 0.6)'
        };

        let labels = [];
        data.forEach(run => { labels.push(`#${run.id}`) });

        let datasets = [];
        for (let state in states) {
            if (states.hasOwnProperty(state)) {
                let set_data = [];
                data.forEach(run => {
                    set_data.push(run[state])
                });
                datasets.push({
                    data: set_data,
                    label: state,
                    backgroundColor: states[state],
                    borderColor: states[state],
                    fill: false
                })
            }
        }

        this.render_chart(elementId, labels, datasets);
    }

    render_chart(elementId, labels, datasets) {
        new Chart(document.getElementById(elementId).getContext('2d'), {
            type: 'line',
            data: {
                labels: labels,
                datasets: datasets
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false
                }
            }
        });
    }

    renderTags(elementId, tags) {
        let html = ''
        tags.forEach(tag => { html += renderTag(tag, false); })
        document.getElementById(elementId).innerHTML = html;
    }

    renderDatatabe() {
        $('#runHistoryTable').DataTable( {
            responsive: true,
            order: [[0, 'desc']],
            pagination: false,
            columnDefs: [{ targets: 'no-sort', orderable: false }],
            dom: '<"row" <"col-md-6"><"col-md-6"f>>t<"row"<"col-md-4"B><"col-md-4"i><"col-md-4">>',
            buttons: [
                {
                    extend: 'csv',
                    fieldSeparator: ';',
                    className: 'btn-sm',
                    text: '<em class="fa fa-download"></em> CSV'
                }
            ]
        } );
    }
}