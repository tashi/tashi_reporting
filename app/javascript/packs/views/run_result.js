'use strict';

import Chart from 'chart.js'
import $ from 'jquery'

import vis from 'vis-network/dist/vis-network'
import {DataSet, Timeline} from 'vis-timeline/standalone'
require('vis-timeline/dist/vis-timeline-graph2d.min.css')

require('datatables.net')
require("datatables.net-buttons")
require("datatables.net-buttons/js/buttons.html5")
require("datatables.net-buttons-bs4")

import {renderSummary, renderTag} from '../utils/functions'

export default class RunResult {

    constructor(runId) {
        this.runId = runId;
    };

    /**
     * Generates a doughnut chart in the given canvas element using chart.js
     * @param elementId (String) the HTML-ID of the canvas to use
     * @param labels (Array) the labels for the areas
     * @param data (Array) the data points labels for the areas
     */
    render_doughnut(elementId, labels, data) {
        new Chart(document.getElementById(elementId).getContext('2d'), {
            type: 'doughnut',
            data: {
                labels: labels,
                datasets: [
                    {
                        backgroundColor: ['#d5d5d5', '#00d500', '#d0a000', '#dd0000'],
                        data: data,
                        datalabels: {
                            display: false
                        }
                    }
                ]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });
    }

    render_group_dependency(elementId) {
        $.get(`/groups/dependencies/${this.runId}.json`, function (data) {
          let parents = [];
          let links = [];
          for(let entry in data) {
              if(data.hasOwnProperty(entry)) {
                  if(!parents.some( p => p.id === data[entry].parent.id )) {
                      parents.push({
                          id: data[entry].parent.id,
                          label: data[entry].parent.name,
                          shape: "box",
                          color: "#C0C0C0"
                      })
                  }
                  if(data[entry].child !== null) {
                      links.push({from: data[entry].parent.id, to: data[entry].child.id})
                  }
              }
          }

          new vis.Network(document.getElementById(elementId),
            {
                nodes: new vis.DataSet(parents),
                edges: new vis.DataSet(links)
            },
            {});
        })
    }

    _render_test_timeline(data, view) {
       let groups = [];
       let tests = [];
       data.forEach(entry => {
           if(!groups.some(g => g.id === entry.group )) {
               groups.push({
                   id: entry.group,
                   content: entry.group
               })
           }
           tests.push({
               group: entry.group,
               content: entry.procedure,
               start: entry.start_time,
               end: entry.end_time,
               className: entry.css,
               title: `${entry.procedure} [${entry.result}]<br /><small>${entry.start_time} - ${entry.end_time}</small>`
           })
       })

        let container = document.getElementById('testExecutionTimeline');
        new Timeline(container, new DataSet(tests), new DataSet(groups), { editable: false });
    }

    _render_results_table(data, view) {
        let self = this;
        $("#runResultsTable").DataTable({
            data: data,
            paging: false,
            order: [[3, 'desc']],
            responsive: true,
            columns: [
                {
                    data: "group",
                    title: "Group"
                },
                {
                    data: "procedure",
                    title: "Procedure",
                    className: 'cell-bold'
                },
                {
                    data: "start_time",
                    title: "Started"
                },
                {
                    data: "end_time",
                    title: "Finished"
                },
                {
                    data: "result",
                    title: "Result",
                    createdCell: (td, _cellData, row) => { $(td).addClass(row.css); }
                },
                {
                    data: 'summary',
                    title: "Summary",
                    render: (cellData) => { return renderSummary(cellData) }
                },
                {
                    data: 'comments',
                    title: "Comments"
                },
                {
                    data: 'tags',
                    title: "Tags",
                    render: (cell) => { return self.renderTags(cell); }
                },
                {
                    data: null,
                    title: "",
                    orderable: false,
                    render: (row) => {
                        return `<a href="/results/${view.runId}/${row.procedure}" title="Detail Results for '${row.procedure}'"><em class="fa fa-file-medical-alt"></em></a>`
                    }
                }
            ],
            dom: 'ft<"row"<"col-md-4"B><"col-md-4"i><"col-md-4"p>>', //'frtipB',
            buttons: [
                {
                    extend: 'csv',
                    fieldSeparator: ';',
                    className: 'btn-sm',
                    text: '<em class="fa fa-download"></em> CSV'
                }
            ]
        });
    }

    /**
     * Fetch results as JSON form the backend and render a datatable
     */
    render_results() {
        let view = this;
        $.get(`/results/${this.runId}.json`, function (data) {
            view._render_results_table(data, view);
            view._render_test_timeline(data, view);
        });
    }

    renderTags(tags) {
        let html = ''
        tags.forEach(tag => { html += renderTag(tag, false); })
        return html;
    }
}