import 'datatables.net'
import {toggleHidden} from "../utils/functions"
import vis from 'vis-network/dist/vis-network'
import Flash from "../utils/flash";

require('datatables.net-bs4')
require("datatables.net-bs4/css/dataTables.bootstrap4.min.css")
require("datatables.net-buttons")
require("datatables.net-buttons/js/buttons.html5")
require("datatables.net-buttons-bs4")

let AutoComplete = require("autocomplete-js");

export default class Requirements {

    toggleVisibility(elementId) {
        toggleHidden(elementId)
    }

    bindSubmitBtn() {
        $(`#submitBtn`).click(() => { this.submitForm(''); })
    }

    submitForm(reqId) {
        this.submitGenericForm(`#reqForm${reqId}`);
    }

    submitGenericForm(selector) {
        let form = $(selector);
        $.post(form.attr('action'), form.serialize()).fail((data) => { Flash.error(data.responseText); });
    }

    activateImportForm() {
        $(".custom-file-input").on("change", function() {
            let input = $(this);
            input.siblings(".custom-file-label").addClass("selected").html(input.val().split("\\").pop());
        });
    }

    showEditModal(reqId, reqRef) {
        $.get(`/requirements/${reqId}/edit`)
            .done((data) => {
                $("#modalTitle").replaceWith(`<span id="modalTitle">Edit '${reqRef}'</span>`);
                $("#modalBody").replaceWith(`<div id="modalBody">${data}</div>`);
                $("#reqEditModal").modal('show');
                $(`#submitBtn${reqId}`).click(() => { this.submitForm(reqId); })
                AutoComplete();
            })
            .fail((data) => { Flash.error(data.responseText); });
    }

    linkProcedure(req, reqId, target) {
        let proc = document.getElementById('newProc').value;
        $.get(`/requirements/link?dir=procToReq&req=${encodeURIComponent(req)}&proc=${encodeURIComponent(proc)}`)
            .done((data) => {
                let row = `<tr id="proc-${data.id}"><td class="cell-bold">${data.name}</td><td class="text-right">`;
                row += `<a href='#' onclick="view.unlinkProcedure(${reqId}, ${data.id})" class="btn btn-danger btn-sm" title="Unlink"><em class="fa fa-unlink"></em></a>`;
                row += `</td></tr>`;
                document.getElementById(target).innerHTML += row;
            })
            .fail((data) => {
                Flash.error(data.responseText);
            });
    }

    unlinkProcedure(reqId, procId) {
        $.get(`/requirements/unlink?req=${reqId}&proc=${procId}`)
            .done(() => { document.getElementById(`proc-${procId}`).remove(); })
            .fail((data) => { Flash.error(data.responseText); });
    }

    linkReq(proc, procId, target) {
        let req = document.getElementById('newReq').value;
        $.get(`/requirements/link?dir=reqToProc&req=${encodeURIComponent(req)}&proc=${encodeURIComponent(proc)}`)
            .done((data) => {
                let row = `<tr id="spr-${data.id}"><td class="cell-bold">${data.reference}</td>`;
                row += `<td>${data.title}</td><td>${data.severity}</td><td>${data.status}</td><td class="text-right">`;
                row += `<a href='#' onclick="req.unlinkReq(${procId}, ${data.id})" class="btn btn-danger btn-sm" title="Unlink"><em class="fa fa-unlink"></em></a>`;
                row += `</td></tr>`;
                document.getElementById(target).innerHTML += row;
            })
            .fail((data) => {
                Flash.error(data.responseText);
            });
    }

    unlinkReq(procId, reqId) {
        $.get(`/requirements/unlink?req=${reqId}&proc=${procId}`)
            .done(() => { document.getElementById(`spr-${reqId}`).remove(); })
            .fail((data) => { Flash.error(data.responseText); });
    }

    openReqMatrix() {
        let btn = document.getElementById('showMatrixBtn')
        let linkText = btn.innerHTML;
        btn.innerHTML = '<span id="stateSpinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ' + linkText;
        $.get('/requirements/matrix')
            .done((data) => {
                $('#reqMatrixModal').modal('show')
                this.renderReqMatrix(data);
            })
            .fail((data) => { Flash.error(data.responseText); })
            .always(() => { btn.innerHTML = linkText; });
    }

    renderReqMatrix(data) {
        let nodes = [];
        let links = [];
        for(let entry in data) {
            if(data.hasOwnProperty(entry)) {
                let req = data[entry].req;
                let procs = data[entry].procedures;
                if(!nodes.some( p => p.id === `req-${req.id}` )) {
                    nodes.push({
                        id: `req-${req.id}`,
                        label: req.reference,
                        shape: "box",
                        color: "#ECA020"
                    })
                }
                if(procs !== null) {
                    for(let proc in procs) {
                        if(procs.hasOwnProperty(proc)) {
                            proc = procs[proc];
                            if(!nodes.some( p => p.id === `proc-${proc.id}`  )) {
                                nodes.push({
                                    id: `proc-${proc.id}`,
                                    label: proc.name,
                                    shape: "box",
                                    color: "#097709"
                                })
                            }
                            if(!nodes.some( l => (l.from === `req-${req.id}` && l.to === `proc-${proc.id}`) )) {
                                links.push({from: `req-${req.id}`, to: `proc-${proc.id}`})
                            }
                        }
                    }
                }
            }
        }

        new vis.Network(document.getElementById('reqMatrix'),
            {
                nodes: new vis.DataSet(nodes),
                edges: new vis.DataSet(links)
            }, {});
    }

    drawTable() {
        $("#requirementsOverview").DataTable({
            order: [[2, 'desc']],
            responsive: true,
            processing: true,
            serverSide: true,
            lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "All"]],
            ajax: '/requirements.json',
            columns: [
                {
                    data: "category",
                    name: "requirement_categories.name",
                    title: "Category"
                },
                {
                    data: "group",
                    name: "requirement_groups.name",
                    title: "Group"
                },
                {
                    data: "reference",
                    className: 'cell-bold',
                    title: "Reference"
                },
                {
                    data: "title",
                    title: "Title"
                },
                {
                    data: "severity",
                    className: 'cell-bold',
                    title: "Severity"
                },
                {
                    data: "status",
                    className: 'cell-bold',
                    title: "Status"
                },
                {
                    data: "description",
                    title: "Description"
                },
                {
                    data: "procedures",
                    title: "Procedures",
                    render: (data) => {
                        return data.join(', ');
                    }
                },
                {
                    data: "updated_at",
                    title: "Updated"
                },
                {
                    data: null,
                    title: "",
                    className: 'options-cell text-right',
                    orderable: false,
                    render: (row) => {
                        let links = `<button class="btn btn-primary btn-sm" onclick="view.showEditModal(${row.id}, '${row.reference}')" title="Edit Requirement"><em class="fa fa-edit"></em></button>`;
                        links += `&nbsp;<a href="/requirements/delete?delid=${row.id}" data-confirm="Please confirm Deletion of Requirement '${row.reference}'." class="btn btn-danger btn-sm" title="Delete Requirement"><em class="fa fa-remove"></em></a>`;
                        return links;
                    }
                }
            ],
            dom: '<"row" <"col-md-6"l><"col-md-6"f>>t<"row"<"col-md-4"B><"col-md-4"i><"col-md-4"p>>',
            buttons: [
                {
                    extend: 'csv',
                    fieldSeparator: ';',
                    className: 'btn-sm',
                    text: '<em class="fa fa-download"></em> CSV'
                }
            ]
        });
    }

}