'use strict';

import $ from 'jquery'
import 'datatables.net'
import {renderTag} from "../utils/functions";
import Flash from "../utils/flash";

require('datatables.net-bs4')
require("datatables.net-bs4/css/dataTables.bootstrap4.min.css")
require("datatables.net-buttons")
require("datatables.net-buttons/js/buttons.html5")
require("datatables.net-buttons-bs4")


export default class ProcOverview {

    check_states() {
        let linkText = document.getElementById('checkStateBtn').innerHTML;
        let spinner = '<span id="stateSpinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ';
        document.getElementById('checkStateBtn').innerHTML = spinner + linkText;
        $.get('/procedures/check_states')
            .done((data) => { Flash.success(data.msg); console.log(data)})
            .fail((data) => { Flash.error(data.responseText); })
            .always(() => { document.getElementById('checkStateBtn').innerHTML = linkText; });

    }

    draw_table() {
        let self = this;
        $("#proceduresOverview").DataTable({
            order: [[0, 'desc']],
            responsive: true,
            processing: true,
            serverSide: true,
            lengthMenu: [ 20, 40, 60, 80, 100 ],
            ajax: '/procedures.json',
            columns: [
                {
                    data: 'id',
                    title: "ID",
                    className: 'cell-bold',
                    render: (cellData, _unused, row) => {
                        return `<a href="/procedures/${row.name}" title="Test Procedure #${row.id}: '${row.name}'">#${cellData}</a>`
                    }
                },
                {
                    data: "name",
                    title: "Name",
                    className: 'cell-bold',
                    render: (cellData) => {
                        return `<a href="/procedures/${cellData}" title="Test Procedure '${cellData}'">${cellData}</a>`
                    }
                },
                {
                    data: "description",
                    title: "Description"
                },
                {
                    data: "pass_fail",
                    title: "Pass/Fail Criteria"
                },
                {
                    data: 'tags',
                    name: 'tags.name',
                    title: "Tags",
                    render: (cell) => { return self.renderTags(cell); }
                },
                {
                    data: 'status.name',
                    name: 'review_states.name',
                    title: "Status"
                },
                {
                    data: "updated_at",
                    title: "Last Changed"
                },
                {
                    data: null,
                    title: "",
                    className: 'cell-bold',
                    orderable: false,
                    render: (row) => {
                        return `<a href="/procedures/${row.name}" title="Show '${row.name}'"><em class="fa fa-file-medical-alt"></em></a>`;
                    }
                }
            ],
            dom: '<"row" <"col-md-6"l><"col-md-6"f>>t<"row"<"col-md-4"B><"col-md-4"i><"col-md-4"p>>', //'frtipB',
            buttons: [
                {
                    extend: 'csv',
                    fieldSeparator: ';',
                    className: 'btn-sm',
                    text: '<em class="fa fa-download"></em> CSV'
                }
            ]
        });
    }

    renderTags(tags) {
        let html = ''
        tags.forEach(tag => { html += renderTag(tag, false); })
        return html;
    }

}