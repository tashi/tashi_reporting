import 'datatables.net'
import Flash from "../utils/flash";
import vis from 'vis-network/dist/vis-network'
import {toggleHidden} from "../utils/functions"

require('datatables.net-bs4')
require("datatables.net-bs4/css/dataTables.bootstrap4.min.css")
require("datatables.net-buttons")
require("datatables.net-buttons/js/buttons.html5")
require("datatables.net-buttons-bs4")

let AutoComplete = require("autocomplete-js");

export default class Spr {

    submitGenericForm(selector) {
        let form = $(selector);
        $.post(form.attr('action'), form.serialize()).fail((data) => { Flash.error(data.responseText); });
    }

    activateImportForm() {
        $(".custom-file-input").on("change", function() {
            let input = $(this);
            input.siblings(".custom-file-label").addClass("selected").html(input.val().split("\\").pop());
        });
    }

    toggleVisibility(elementId) {
        toggleHidden(elementId)
    }

    bindSubmitBtn() {
        $(`#submitBtn`).click(() => { this.submitForm(''); })
    }

    showEditModal(sprId, sprRef) {
        $.get(`/sprs/${sprId}/edit`)
            .done((data) => {
                $("#modalTitle").replaceWith(`<span id="modalTitle">Edit '${sprRef}'</span>`);
                $("#modalBody").replaceWith(`<div id="modalBody">${data}</div>`);
                $("#sprEditModal").modal('show');
                $(`#submitBtn${sprId}`).click(() => { this.submitForm(sprId); })
                AutoComplete();
            })
            .fail((data) => { Flash.error(data.responseText); });
    }

    submitForm(sprId) {
        let form = $(`#sprForm${sprId}`);
        $.post(form.attr('action'), form.serialize()).fail((data) => { Flash.error(data.responseText); });
    }

    linkSpr(proc, procId, target) {
        let spr = document.getElementById('newSpr').value;
        $.get(`/sprs/link?dir=sprToProc&spr=${encodeURIComponent(spr)}&proc=${encodeURIComponent(proc)}`)
            .done((data) => {
                let row = `<tr id="spr-${data.id}"><td class="cell-bold">${data.reference}</td>`;
                row += `<td>${data.severity}</td><td>${data.title}</td><td class="text-right">`;
                row += `<a href='#' onclick="spr.unlinkSpr(${procId}, ${data.id})" class="btn btn-danger btn-sm" title="Unlink"><em class="fa fa-unlink"></em></a>`;
                row += `</td></tr>`;
                document.getElementById(target).innerHTML += row;
            })
            .fail((data) => {
                Flash.error(data.responseText);
            });
    }

    unlinkSpr(procId, sprId) {
        $.get(`/sprs/unlink?spr=${sprId}&proc=${procId}`)
            .done(() => { document.getElementById(`spr-${sprId}`).remove(); })
            .fail((data) => { Flash.error(data.responseText); });
    }

    linkProcedure(spr, sprId, target) {
        let proc = document.getElementById('newProc').value;
        $.get(`/sprs/link?dir=procToSpr&spr=${encodeURIComponent(spr)}&proc=${encodeURIComponent(proc)}`)
            .done((data) => {
                let row = `<tr id="proc-${data.id}"><td class="cell-bold">${data.name}</td><td class="text-right">`;
                row += `<a href='#' onclick="view.unlinkProcedure(${sprId}, ${data.id})" class="btn btn-danger btn-sm" title="Unlink"><em class="fa fa-unlink"></em></a>`;
                row += `</td></tr>`;
                document.getElementById(target).innerHTML += row;
            })
            .fail((data) => {
                Flash.error(data.responseText);
            });
    }

    unlinkProcedure(sprId, procId) {
        $.get(`/sprs/unlink?spr=${sprId}&proc=${procId}`)
            .done(() => { document.getElementById(`proc-${procId}`).remove(); })
            .fail((data) => { Flash.error(data.responseText); });
    }

    openSprMatrix() {
        let btn = document.getElementById('showSprMatrixBtn')
        let linkText = btn.innerHTML;
        btn.innerHTML = '<span id="stateSpinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ' + linkText;
        $.get('/sprs/matrix')
            .done((data) => {
                $('#sprMatrixModal').modal('show')
                this.renderSprMatrix(data);
            })
            .fail((data) => { Flash.error(data.responseText); })
            .always(() => { btn.innerHTML = linkText; });
    }

    renderSprMatrix(data) {
        let nodes = [];
        let links = [];
        for(let entry in data) {
            if(data.hasOwnProperty(entry)) {
                let spr = data[entry].spr;
                let procs = data[entry].procedures;
                if(!nodes.some( p => p.id === `spr-${spr.id}` )) {
                    nodes.push({
                        id: `spr-${spr.id}`,
                        label: spr.reference,
                        shape: "box",
                        color: "#ECA020"
                    })
                }
                if(procs !== null) {
                    for(let proc in procs) {
                        if(procs.hasOwnProperty(proc)) {
                            proc = procs[proc];
                            if(!nodes.some( p => p.id === `proc-${proc.id}`  )) {
                                nodes.push({
                                    id: `proc-${proc.id}`,
                                    label: proc.name,
                                    shape: "box",
                                    color: "#097709"
                                })
                            }
                            if(!nodes.some( l => (l.from === `spr-${spr.id}` && l.to === `proc-${proc.id}`) )) {
                                links.push({from: `spr-${spr.id}`, to: `proc-${proc.id}`})
                            }
                        }
                    }
                }
            }
        }

        new vis.Network(document.getElementById('sprMatrix'),
            {
                nodes: new vis.DataSet(nodes),
                edges: new vis.DataSet(links)
            }, {});
    }

    drawTable() {
        let self = this;
        $("#sprsOverview").DataTable({
            order: [[0, 'desc']],
            responsive: true,
            processing: true,
            serverSide: true,
            lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "All"]],
            ajax: '/sprs.json',
            columns: [
                {
                    data: "reference",
                    className: 'cell-bold',
                    title: "Reference"
                },
                {
                    data: "severity",
                    className: 'cell-bold',
                    title: "Severity"
                },
                {
                    data: "title",
                    title: "Title"
                },
                {
                    data: "description",
                    title: "Description"
                },
                {
                    data: "procedures",
                    title: "Procedures",
                    render: (data) => {
                        return data.join(', ');
                    }
                },
                {
                    data: "updated_at",
                    title: "Updated"
                },
                {
                    data: null,
                    title: "",
                    className: 'text-right',
                    orderable: false,
                    render: (row) => {
                        let links = ` <button class="btn btn-primary btn-sm" onclick="view.showEditModal(${row.id}, '${row.reference}')" title="Edit SPR"><em class="fa fa-edit"></em></button>`;
                        links += ` <a href="/sprs/delete?delid=${row.id}" data-confirm="Please confirm Deletion of SPR '${row.reference}'." class="btn btn-danger btn-sm" title="Delete SPR"><em class="fa fa-remove"></em></a>`;
                        return links;
                    }
                }
            ],
            dom: '<"row" <"col-md-6"l><"col-md-6"f>>t<"row"<"col-md-4"B><"col-md-4"i><"col-md-4"p>>',
            buttons: [
                {
                    extend: 'csv',
                    fieldSeparator: ';',
                    className: 'btn-sm',
                    text: '<em class="fa fa-download"></em> CSV'
                }
            ]
        });
    }
}