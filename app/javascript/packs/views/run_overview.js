'use strict';

import $ from 'jquery'
import 'datatables.net'

require('datatables.net-bs4')
require("datatables.net-bs4/css/dataTables.bootstrap4.min.css")
require("datatables.net-buttons")
require("datatables.net-buttons/js/buttons.html5")
require("datatables.net-buttons-bs4")

import Chart from 'chart.js'
import ChartDataLabels from 'chartjs-plugin-datalabels'

export default class RunOverview {

    /**
     * Constructor
     * @param colors {object} JSON object with {'reslt name': '#HEXCODE'} to define colors for charts
     */
    constructor(colors) {
        this.colors = colors;
    };

    draw_table() {
        let view = this;
        $("#resultsOverview").DataTable({
            order: [[0, 'desc']],
            responsive: true,
            processing: true,
            serverSide: true,
            lengthMenu: [ 20, 40, 60, 80, 100 ],
            ajax: '/results.json',
            columns: [
                {
                    data: 'id',
                    title: "ID",
                    className: 'cell-bold',
                    render: (cellData) => {
                        return `<a href="/results/${cellData}" title="Detail Results for run '#${cellData}'">#${cellData}</a>`
                    }
                },
                {
                    data: "name",
                    title: "Name"
                },
                {
                    data: "environment",
                    title: "Environment"
                },
                {
                    data: "specification",
                    title: "Test Spec"
                },
                {
                    data: "started",
                    title: "Started"
                },
                {
                    data: "finished",
                    title: "Finished"
                },
                {
                    data: "duration",
                    title: "Duration",
                    orderable: false,
                },
                {
                    data: "total_procs",
                    title: "TP",
                    orderable: false,
                },
                {
                    data: null,
                    title: "Summary",
                    visible: false,
                    orderable: false,
                    render: (row) => { return JSON.stringify(row.summary) }
                },
                {
                    data: null,
                    title: "Results",
                    orderable: false,
                    render: view.chartHtml
                },
                {
                    data: null,
                    title: "",
                    className: 'cell-bold',
                    orderable: false,
                    render: (row) => {
                        let links = `<a href="/results/${row.id}" title="Detail Results for run '#${row.id}'"><em class="fa fa-file-medical-alt"></em></a> | `;
                        links += ` <a href="/results/${row.id}/history" title="History for '#${row.id}'"><em class="fa fa-history"></em></a> | `;
                        links += ` <a href="/results/${row.id}/spr_coverage" title="SPR Coverage for '#${row.id}'"><em class="fa fa-bug"></em></a> | `;
                        links += ` <a href="/results/${row.id}/req_coverage" title="Requirements Coverage for '#${row.id}'"><em class="fa fa-space-shuttle"></em></a>`;
                        return links;
                    }
                }
            ],
            dom: '<"row" <"col-md-6"l><"col-md-6"f>>t<"row"<"col-md-4"B><"col-md-4"i><"col-md-4"p>>',
            buttons: [
                {
                    extend: 'csv',
                    fieldSeparator: ';',
                    className: 'btn-sm',
                    text: '<em class="fa fa-download"></em> CSV'
                }
            ]
        });
    }

    chartHtml(row) {
        let html = "<div class=\"chart-container\" style=\"width:150px; height: 25px\">"
        html += `<canvas style="width: 150px !important; height: 25px !important;" id="resultStack_${row.id}"></canvas>`
        html += `<script>view.render_stack('resultStack_${row.id}', ${JSON.stringify(row.summary)});</script>`
        html += "</div>"
        return html;
    }

    /**
     * Render a stack chart for the results
     * @param elementId {string} the HTML-ID of the canvas
     * @param data {object} a JSON data object with {'label': count} for each result
     */
    render_stack(elementId, data) {
        new Chart(document.getElementById(elementId).getContext('2d'), {
            plugins: [ChartDataLabels],
            type: 'horizontalBar',
            data: {
                labels: ['Results'],
                datasets: this._genDataSet(data),
            },
            options: this._defaultOptions()
        });
    }

    _genDataSet(data) {
        let datasets = []
        for (let entry in data) {
            if (data.hasOwnProperty(entry)) {
                datasets.push({
                    label: entry,
                    backgroundColor: this.colors[entry],
                    data: [data[entry]],
                    datalabels: {
                        align: 'center',
                        anchor: 'center',
                        display: data[entry] > 0,
                        color: '#343434',
                        font: {size: '11', weight: 'bold'}
                    }
                })
            }
        }
        return datasets;
    }

    _defaultOptions() {
        return {
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        display: false
                    },
                    stacked: true
                }],
                yAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        display: false
                    },
                    stacked: true
                }]
            }
        }
    }

}