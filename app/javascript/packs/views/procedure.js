'use strict';

import $ from 'jquery'
import Chart from 'chart.js'
import Flash from "../utils/flash";
import {scrollDown} from "../utils/functions";


export default class Procedure {

    constructor(procId) {
        this.procId = procId;
    };

    /**
     * Add a new comment
     * @param formId the HTML id of the form to send
     * @param tableId the HTML id of the table to append the new comment to
     */
    add_review(formId, tableId) {
        let self = this;
        let form = $(`#${formId}`);
        $.post('/reviews', form.serialize(), () => {})
            .done((data) => {
                self.append_review(tableId, data);
                form.trigger('reset');
            })
            .fail((data) => { Flash.error(data.responseText); });
    }

    /**
     * Removes a content from the given table and adds comments for the current procedure result
     * @param tableId {String} the id of the table HTML element
     */
    render_reviews(tableId) {
        $.get(`/reviews/${this.procId}`, (data) => {
            document.getElementById(tableId).innerHTML = '';
            data.forEach(review => { this.append_review(tableId, review); });
        });
    }

    /**
     * Appends the comment a s row to the given table
     * @param tableId {String} the id of the table HTML element
     * @param review {JSON} the review to append
     */
    append_review(tableId, review) {
        let innerHTML = document.getElementById(tableId).innerHTML;
        document.getElementById(tableId).innerHTML = innerHTML + this.review_row(review);
        scrollDown('scroll-box');
    }

    /**
     * Generates HTML for the given comment
     * @param review {JSON} the review in JSON format
     * @return {String} the generated HTML
     */
    review_row(review) {
        let html = '<tr>';
        html += `<td>${review.created_at}</td>`;
        html += `<td>${review.user}</td>`;
        html += `<td class="cell-bold">[${review.status}]</td>`;
        html += `<td>${review.text}</td>`;
        html += '</tr>';
        return html;
    }

    /**
     * Generates a area chart in the given canvas element using chart.js
     * @param elementId (String) the HTML-ID of the canvas to use
     */
    render_weekly(elementId) {
        $.get(`/procedures/${this.procId}/weekly_history`, function (data) {
            let states = {
                'passed': 'rgba(0, 255, 0, 0.45)',
                'observations': 'rgba(255, 230, 0, 0.6)',
                'failed': 'rgba(255, 0, 0, 0.6)',
                'not_exec': 'rgba(150, 150, 150, 0.6)'
            };
            let times = [];
            for (let time in data['passed']) {
                if (data['passed'].hasOwnProperty(time)) {
                    times.push(time);
                }
            }

            let datasets = [];
            for (let state in states) {
                if (states.hasOwnProperty(state)) {
                    let set_data = [];
                    times.forEach(time => {
                        set_data.push(data[state][time])
                    })
                    datasets.push({
                        data: set_data,
                        label: state,
                        backgroundColor: states[state],
                        borderColor: states[state],
                        fill: false
                    })
                }
            }

            new Chart(document.getElementById(elementId).getContext('2d'), {
                type: 'line',
                data: {
                    labels: times,
                    datasets: datasets
                },
                options: {
                    legend: {
                        display: true
                    }
                }
            });
        });
    }

    /**
     * Generates a doughnut chart in the given canvas element using chart.js
     * @param elementId (String) the HTML-ID of the canvas to use
     */
    render_total(elementId) {
        let self = this;
        $.get(`/procedures/${this.procId}/total_results`, (data) => { self.render_total_with_data(elementId, data) });
    }

    /**
     * Generates a doughnut chart in the given canvas element using chart.js
     * @param elementId (String) the HTML-ID of the canvas to use
     * @param data (Object) the data object to use with {label: data}
     */
    render_total_with_data(elementId, data) {
            let labels = [];
            let points = [];
            for (let entry in data) {
                if (data.hasOwnProperty(entry)) {
                    labels.push(entry);
                    points.push(data[entry])
                }
            }

            new Chart(document.getElementById(elementId).getContext('2d'), {
                type: 'doughnut',
                data: {
                    labels: labels,
                    datasets: [
                        {
                            backgroundColor: ['#00d500', '#d0a000', '#dd0000', '#d5d5d5'],
                            data: points,
                        }
                    ]
                },
                options: {
                    legend: {
                        display: true
                    }
                }
            });
    }

}