/**
 * Replaces '"', '&'. '<' and '>' characters with HTML safe chars in given text
 * @param text {string} the text to replace
 * @returns {string}
 */
function escape(text) {
    return text.replace(/[\"&<>]/g, function (a) {
        return { '"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;' }[a];
    });
}

/**
 * Generates HTML based on the JSON test procedure summary
 * @param {Object} data the JSON summary of a test procedure
 * @returns {string} the generated HTML string
 */
function renderSummary(data) {
    let html = [];
    for (let step in data) {
        if (data.hasOwnProperty(step)) {
            html.push(`[Step: <strong>${escape(step)}</strong>]`);
            let messages = data[step].observations.concat(data[step].failures);
            messages.forEach(msg => html.push(escape(msg)));
        }
    }
    return html.join('<br />');
}

/**
 * Generates an HTML representation from a tag in JSON
 * @param {Object} tag
 * @param {Boolean} deleteButton flag to determine if a delete button shall be displayed
 * @returns {string} the generated HTML string
 */
function renderTag(tag, deleteButton) {
    if (typeof deleteButton === 'undefined' || deleteButton === null)  {
        deleteButton = false;
    }
    let html = `<small class='btn btn-sm btn-tag' style='background-color: ${tag.color} !important; color: ${tag.text_color} !important; cursor: default;' title='${tag.desc}' id="tag-${tag.id}">${tag.name}`;
    if(deleteButton) { html += `&nbsp;<button type="button" class="close small btn-tag-del" style='position: relative; bottom: 4px;' title="Remove ${tag.name}" id="rm-tag-${tag.id}">x</button>`; }
    return html + `</small><span class="sr-only">,</span> `;
}

function scrollDown(className) {
    Array.from(document.getElementsByClassName(className)).forEach(element => {element.scrollTop = element.scrollHeight;});
}

function toggleHidden(elementId) {
    let element = document.getElementById(elementId);
    if (element.classList.contains('hidden')) {
        element.classList.remove('hidden');
    } else {
        element.classList.add('hidden');
    }
}

export {escape, renderSummary, renderTag, scrollDown, toggleHidden};