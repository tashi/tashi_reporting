'use strict';

export default class Flash {

    static success(msg) {
        let html = '<div class="alert alert-success alert-dismissable fade show" role="alert">'
        html += `<em class="fa fa-check-circle"></em> ${msg}`
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        html += '<span aria-hidden="true">&times;</span></button></div>'

        let original = document.getElementById('flash').innerHTML;
        document.getElementById('flash').innerHTML= original + html;
    }

    static error(msg) {
        let html = '<div class="alert alert-danger alert-dismissable fade show" role="alert">'
        html += `<em class="fa fa-times-circle"></em> ${msg}`
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        html += '<span aria-hidden="true">&times;</span></button></div>'

        let original = document.getElementById('flash').innerHTML;
        document.getElementById('flash').innerHTML= original + html;
    }

}