# frozen_string_literal: true

# Datatable backend for server side processing
# for test runs.
#
# Author johannes.hoelken@hawk.de
class ProceduresOverviewTable < ApplicationDatatable
  protected

  # rubocop:disable Metrics/MethodLength
  def data
    model.map do |proc|
      {
        id: proc.id,
        name: proc.name,
        description: proc.description,
        pass_fail: proc.pass_fail,
        updated_at: proc.updated_at,
        tags: proc.tags.as_json,
        status: proc.review_state
      }
    end
  end
  # rubocop:enable Metrics/MethodLength

  def joined_model
    model_class.left_outer_joins(:tags, :review_state)
  end

  def model_class
    Procedure
  end

  def columns
    %w[
      procedures.id
      procedures.name
      procedures.description
      procedures.pass_fail
      procedures.updated_at
      tags.name
      review_states.name
    ]
  end
end
