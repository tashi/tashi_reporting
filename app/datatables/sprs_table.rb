# frozen_string_literal: true

# Datatable backend for server side processing
# for test runs.
#
# Author johannes.hoelken@hawk.de
class SprsTable < ApplicationDatatable
  protected

  # rubocop:disable Metrics/MethodLength
  def data
    model.map do |spr|
      {
        id: spr.id,
        reference: spr.reference,
        severity: spr.severity,
        title: spr.title,
        description: spr.description,
        procedures: spr.procedures.select(:name).map(&:name),
        updated_at: spr.updated_at
      }
    end
  end
  # rubocop:enable Metrics/MethodLength

  def model_class
    Spr
  end

  def joined_model
    model_class.left_outer_joins(:procedures)
  end

  def total_entries
    with_custom_search(joined_model).distinct.count('sprs.id')
  end

  def columns
    %w[sprs.reference sprs.title sprs.severity sprs.description procedures.name sprs.updated_at]
  end
end
