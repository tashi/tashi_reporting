# frozen_string_literal: true

# Base backend class for server side processing of datatables
# Implement +data+, +columns+ and +model_class+ methods.
# You can provide custom search conditions when also overriding
# +with_custom_search(model)+ method.
# If you need to join the model implement +joined_model+ also.
# See documentation of stubs for mor details.
#
# Author johannes.hoelken@hawk.de
class ApplicationDatatable
  attr_reader :params

  def initialize(params)
    @params = params
  end

  def as_json(_options = {})
    {
      data: data,
      recordsTotal: total_entries,
      recordsFiltered: filtered_entries
    }
  end

  protected

  def data
    raise "NOT IMPLEMENTED 'data' on #{self.class.name}."
  end

  def columns
    raise "NOT IMPLEMENTED 'columns' on #{self.class.name}."
  end

  def model_class
    raise "NOT IMPLEMENTED 'model_class' on #{self.class.name}."
  end

  def with_custom_search(model)
    model
  end

  def joined_model
    model_class
  end

  private

  def total_entries
    with_custom_search(joined_model).count
  end

  def filtered_entries
    fetch_model(paging: false).count
  end

  def model
    @model ||= fetch_model
  end

  def fetch_model(paging: true)
    model = joined_model
    model = with_ordering(model)
    model = with_paging(model) if paging
    model = with_datatable_search(model)
    with_custom_search(model).uniq
  end

  def with_ordering(model)
    return model unless sort_column.present?

    model.order("#{sort_column} #{sort_direction}")
  end

  def with_paging(model)
    return model unless params[:length].to_i.positive?

    model.limit(per_page).offset(per_page * page)
  end

  def with_datatable_search(model)
    return model unless params[:search][:value].present?

    model.where(search_condition, search: "%#{params[:search][:value]}%")
  end

  def search_condition
    columns.map { |term| "#{term}::VARCHAR ILIKE :search" }.join(' OR ')
  end

  def page
    @page ||= params[:start].to_i / per_page
  end

  def per_page
    @per_page ||= params[:length].to_i
  end

  def sort_column
    return @sort_col if @sort_col.present?

    order = params[:order]['0'][:column]
    @sort_col = params[:columns][order]['name']
    @sort_col = params[:columns][order]['data'] if @sort_col.blank?
    @sort_col
  end

  def sort_direction
    @sort_direction ||= params[:order]['0'][:dir] == 'desc' ? 'desc' : 'asc'
  end
end
