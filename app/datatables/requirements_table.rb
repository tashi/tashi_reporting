# frozen_string_literal: true

# Datatable backend for server side processing
# for test runs.
#
# Author johannes.hoelken@hawk.de
class RequirementsTable < ApplicationDatatable
  protected

  # rubocop:disable Metrics/MethodLength
  def data
    model.map do |req|
      {
        id: req.id,
        category: req.requirement_category&.name,
        group: req.requirement_group&.name,
        reference: req.reference,
        title: req.title,
        description: req.description,
        severity: req.severity,
        status: req.status,
        procedures: req.procedures.select(:name).map(&:name),
        updated_at: req.updated_at
      }
    end
  end
  # rubocop:enable Metrics/MethodLength

  def model_class
    Requirement
  end

  def joined_model
    model_class.left_outer_joins(:procedures, :requirement_category, :requirement_group)
  end

  def total_entries
    with_custom_search(joined_model).distinct.count('requirements.id')
  end

  def columns
    %w[requirements.reference requirements.title requirements.severity requirements.description procedures.name
       requirements.updated_at requirement_categories.name requirement_groups.name]
  end
end
