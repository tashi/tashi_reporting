# frozen_string_literal: true

# Datatable backend for server side processing
# for test runs.
#
# Author johannes.hoelken@hawk.de
class ResultOverviewTable < ApplicationDatatable
  include ResultHelper

  protected

  # rubocop:disable Metrics/MethodLength
  def data
    model.map do |run|
      {
        id: run.id,
        name: run.name,
        specification: run.specification,
        environment: run.environment,
        started: run.start_time.to_s,
        finished: run.end_time&.to_s,
        duration: duration(run.start_time, run.end_time),
        total_procs: run.results.size,
        summary: run.summary
      }
    end
  end
  # rubocop:enable Metrics/MethodLength

  def model_class
    ReportingTestRun
  end

  def columns
    %w[id name specification environment start_time end_time]
  end
end
