# frozen_string_literal: true

# Datatables backend for server side processing of test procedure results
# Author johannes.hoelken@hawk.de
class ProcHistoryTable < ApplicationDatatable
  include ResultHelper

  def initialize(params, procedure:)
    super(params)
    @procedure = procedure
  end

  protected

  # rubocop:disable Metrics/MethodLength
  # rubocop:disable Metrics/AbcSize
  def data
    model.map do |result|
      {
        test_run_id: result.test_run.id,
        name: result.procedure.name,
        group: result.group.name,
        specification: result.test_run.specification,
        environment: result.test_run.environment,
        start_time: result.start_time.to_s,
        end_time: result.end_time&.to_s,
        duration: duration(result.start_time, result.end_time),
        result: result.result,
        result_css: result.result_css,
        summary: result.summary_data
      }
    end
  end
  # rubocop:enable Metrics/MethodLength
  # rubocop:enable Metrics/AbcSize

  def with_custom_search(model)
    return model if @procedure.blank?

    model.where(procedure_id: Procedure.select('id').find_by!(name: @procedure).id)
  end

  def model_class
    ProcedureResult
  end

  def joined_model
    model_class.left_outer_joins(:test_run, :procedure, :run_group)
  end

  def columns
    %w[
      procedure_results.start_time
      procedure_results.end_time
      procedure_results.result
      procedure_results.message
      run_groups.name
      test_runs.specification
      test_runs.environment
    ]
  end
end
