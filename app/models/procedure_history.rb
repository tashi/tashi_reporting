# frozen_string_literal: true

# Pseudo class to provide insights for procedure executions histories
# Author johannes.hoelken@hawk.de
class ProcedureHistory < ApplicationRecord
  self.table_name = 'procedures'

  RESULTS = {
    passed: Tashi::Result::PASSED,
    observations: Tashi::Result::OBSERVATIONS,
    failed: Tashi::Result::FAILED,
    not_exec: Tashi::Result::NOT_EXECUTED
  }.freeze

  def weekly_stats(lookback: 4.weeks.ago)
    map_data_result(weekly_per_result(lookback: lookback))
  end

  def total_results
    RESULTS.transform_values do |state|
      ProcedureResult.where(procedure_id: id).where('result = ?', state).count
    end
  end

  private

  def map_data_result(data)
    result = empty_weekly_result(data)
    data.each do |state, hash|
      hash.each { |key, value| result[state][key] += value }
    end
    result
  end

  def empty_weekly_result(data)
    RESULTS.keys.each_with_object({}) do |state, result|
      result[state] = {}
      data.each_value { |v| v.each_key { |k| result[state][k] = 0 } }
    end
  end

  def weekly_per_result(lookback:)
    RESULTS.transform_values do |value|
      weekly_count(state: value, lookback: lookback)
    end
  end

  def weekly_count(state:, lookback:)
    ProcedureResult.where('procedure_id = ? AND result = ? AND start_time >= ?', id, state, lookback)
                   .group("DATE_TRUNC('week', start_time)").count
  end
end
