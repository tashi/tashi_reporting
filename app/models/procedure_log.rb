# frozen_string_literal: true

# Pseudo Model to parse log and fetch test files of a procedure execution
# Author johannes.hoelken@hawk.de
class ProcedureLog
  def self.parse(execution)
    ProcedureLog.new(execution).read
  end

  def self.find_files(execution)
    ProcedureLog.new(execution).read(complete: false).files
  end

  attr_reader :logfile, :steps, :files, :base

  def initialize(execution)
    @execution = execution
    @steps = []
    @files = []
  end

  def read(complete: true)
    find_logfile
    parse_log if complete
    find_files
    self
  end

  def logfile_present?
    logfile.present?
  end

  def id
    execution.id
  end

  private

  attr_reader :execution, :format

  def find_logfile
    return unless results_dir_known?

    @base = File.join(execution.test_run.results_dir, execution.procedure.name.parameterize)
    if File.exist?("#{@base}.xml")
      @logfile = "#{base}.xml"
      @format = :xml
    elsif File.exist?("#{@base}.json")
      @logfile = "#{base}.json"
      @format = :json
    end
  end

  def results_dir_known?
    execution.test_run.results_dir.present?
  end

  def parse_log
    return unless logfile_present?

    parse_xml if @format == :xml
    parse_json if @format == :json
  end

  def find_files
    return unless logfile_present?

    @files = find_entries(base)
    @files << { path: Pathname(@logfile), name: "Procedure Execution Logfile (#{format})", folder: false, children: [] }
  end

  # list all children and sub-children of the given folder.
  # @return (Array) Array of Hashes with path:String, name:String, folder:Boolean and children:Array
  def find_entries(dir)
    result = []
    Pathname(dir).each_child do |child|
      next if %w[. ..].include?(child)

      children = child.directory? ? find_entries(child) : []
      result << { path: child, name: child.basename, folder: children.present?, children: children }
    end
    result
  end

  def escape(text)
    text.nil? ? '' : CGI.escapeHTML(text).gsub("\n", '<br />').html_safe
  end

  # rubocop:disable Metrics/MethodLength
  # rubocop:disable Metrics/AbcSize
  def parse_xml
    log = Nokogiri::XML(File.read(@logfile)).root

    log.css('steps step').each do |step|
      @steps << {
        title: step.attribute('name').value,
        duration: step.attribute('duration').value,
        result: step.attribute('result').value,
        instruction: step.css('instruction').text,
        expectation: step.css('expectation').text,
        observation: step.css('observation').text,
        log: escape(step.css('log').text)
      }
    end
  end
  # rubocop:enable Metrics/MethodLength
  # rubocop:enable Metrics/AbcSize

  # rubocop:disable Metrics/MethodLength
  def parse_json
    log = JSON.parse(File.read(@logfile))

    log['steps'].each do |step|
      @steps << {
        title: step['name'],
        duration: step['duration'],
        result: step['result'],
        instruction: step['instruction'],
        expectation: step['expectation'],
        observation: step['summary'],
        log: escape(step['log'])
      }
    end
  end
  # rubocop:enable Metrics/MethodLength
end
