# frozen_string_literal: true

# Pseudo Model to generate spr procedure linking information
#
# Author johannes.hoelken@hawk.de
class SprProcMatrix < MatrixBase
  # Retrieve a link matrix of test procedure to SPR
  def as_json(_options = {})
    generate_json(Spr, :spr)
  end

  # Retrieve a matrix with coverage information per SPR.
  # Will report the results off all linked procedures within the given test run
  def coverage(testrun_id)
    generate_coverage(testrun_id, Spr, :spr)
  end
end
