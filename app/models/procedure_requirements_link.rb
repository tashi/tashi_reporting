# frozen_string_literal: true

# Linking model of Procedures and Requirements
#
# Author johannes.hoelken@hawk.de
class ProcedureRequirementsLink < ApplicationRecord
  belongs_to :requirement
  belongs_to :procedure

  validates :procedure_id, uniqueness: { scope: :requirement_id, message: 'and Requirement already linked.' }
end
