# frozen_string_literal: true

# Requirement Category model
# Author johannes.hoelken@hawk.de
class RequirementGroup < ApplicationRecord
  has_many :requirements, dependent: :destroy

  validates :name, presence: true, uniqueness: true

  def used?
    requirements.exists?
  end
end
