# frozen_string_literal: true

# Extension of tashi/model/test_run
# Author johannes.hoelken@hawk.de
class ReportingTestRun < TestRun
  include ResultHelper

  def summary
    data = Tashi::Result::ALL.each_with_object({}) { |r, summary| summary[r] = 0 }
    results.each_with_object(data) { |result, summary| summary[result.result] = summary.fetch(result.result, 0) + 1 }
  end

  # rubocop:disable Metrics/MethodLength
  # rubocop:disable Metrics/AbcSize
  def result_json
    results.each_with_object([]) do |result, json|
      json << {
        group: result.group.name,
        procedure: result.procedure.name,
        tags: result.procedure.tags.as_json,
        start_time: result.start_time,
        end_time: result.end_time,
        css: "cell-#{result.result_css}",
        result: result.result,
        comments: result.comments.map(&:text).join('; '),
        summary: result.summary_data
      }
    end
  end
  # rubocop:enable Metrics/MethodLength
  # rubocop:enable Metrics/AbcSize
end
