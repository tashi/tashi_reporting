# frozen_string_literal: true

# Namespace for model importer
module Importer
  # CSV Importer for SPRs Model
  #
  # Author johannes.hoelken@hawk.de
  class Sprs < CsvBase
    def self.run(params)
      Importer::Sprs.new(params.except(:action, :controller, :authenticity_token)).import
    end

    private

    def process(row)
      read_import_params(row)
      import_spr if @import_params[:reference]
    end

    def read_import_params(row)
      @import_params = {}
      row.each_with_index { |col, idx| col_to_param(col, idx) }
    end

    def col_to_param(col, index)
      @import_params[mapping[index]] = col if mapping[index]
    end

    def import_spr
      if params[:collision_handling] == 'keep' && Spr.exists?(reference: @import_params[:reference])
        @result[:skipped] += 1
      else
        create_or_update(@import_params)
        @result[:imported] += 1
      end
    end

    def create_or_update(import_params)
      req = Spr.find_or_create_by!(reference: import_params[:reference])
      req.update(import_params)
      req.save!
    end
  end
end
