# frozen_string_literal: true

# Namespace for model importer
module Importer
  # CSV Importer for Requirements Model
  #
  # Author johannes.hoelken@hawk.de
  class Requirements < CsvBase
    def self.run(params)
      Importer::Requirements.new(params.except(:action, :controller, :authenticity_token)).import
    end

    private

    def process(row)
      read_import_params(row)
      import_requirement if @import_params[:reference]
    end

    def read_import_params(row)
      @import_params = {}
      row.each_with_index { |col, idx| col_to_param(col, idx) }
    end

    def col_to_param(col, index)
      return unless mapping[index]

      case mapping[index]
      when :category
        @import_params[:requirement_category] = RequirementCategory.find_or_create_by!(name: col)
      when :group
        @import_params[:requirement_group] = RequirementGroup.find_or_create_by!(name: col)
      else
        @import_params[mapping[index]] = col
      end
    end

    def import_requirement
      if params[:collision_handling] == 'keep' && Requirement.exists?(reference: @import_params[:reference])
        @result[:skipped] += 1
      else
        create_or_update(@import_params)
        @result[:imported] += 1
      end
    end

    def create_or_update(import_params)
      req = Requirement.find_or_create_by!(reference: import_params[:reference])
      req.update(import_params)
      req.save!
    end
  end
end
