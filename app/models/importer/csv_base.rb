# frozen_string_literal: true

# Namespace for model importer
module Importer
  # CSV Base Importer
  #
  # Author johannes.hoelken@hawk.de
  class CsvBase
    def initialize(params)
      @params = params.except(:csv)
      @data = JSON.parse(params[:csv]).keys_as_symbols(recursive: true)
      @result = { imported: 0, skipped: 0 }
    end

    # Kick the actual import
    def import
      headers # shift headers from data
      data.each { |row| process(row) }
      result
    end

    protected

    attr_reader :params, :result, :data

    # Stub to be implemented
    def process(_row)
      raise 'Not Implemented!'
    end

    # On CSV.to_json the headers will become the first array in the new two-dim array
    # representation of the CSV table.
    # With +shift+ we cut the first row off +@data+ and do some post processing afterwards.
    def headers
      @headers ||= data.shift.map { |h| h.encode('UTF-8').parameterize }
    end

    # We will lazy-create a mapping based on the col-position and the selected import target
    def mapping
      @mapping ||= headers.each_with_object([]) { |h, map| map << (params[h] == 'false' ? false : params[h]&.to_sym) }
    end
  end
end
