# frozen_string_literal: true

# Linking model of procedures and Tags
#
# Author johannes.hoelken@hawk.de
class ProcedureTagsLink < ApplicationRecord
  belongs_to :tag
  belongs_to :procedure
end
