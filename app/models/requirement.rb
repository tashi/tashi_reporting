# frozen_string_literal: true

# Requirement model
# Author johannes.hoelken@hawk.de
class Requirement < ApplicationRecord
  has_many :procedure_requirements_links, dependent: :destroy
  has_many :procedures, through: :procedure_requirements_links

  belongs_to :requirement_category, required: false
  belongs_to :requirement_group, required: false

  validates :reference, presence: true, uniqueness: true

  def used?
    procedure_requirements_links.exists?
  end
end
