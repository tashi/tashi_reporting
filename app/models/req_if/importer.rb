# frozen_string_literal: true

class ReqIf
  # ReqIF Importer
  #
  # Takes the +params+ hash from the import configuration form and imports
  # the ReqIF requirements to the internal +Requirements+ model.
  #
  # If the importer finds a field that shall be imported as reference it collects all
  # Data as configured on this and the parent levels.
  # In the complete hierarchy each field may only be used once, and the first mach is used
  # starting from the current level and propagating through the parents.
  #
  # Author johannes.hoelken@hawk.de
  #
  class Importer
    def self.run(params)
      ReqIf::Importer.new(params.except(:action, :controller, :authenticity_token)).import
    end

    def initialize(params)
      @params = params.except(:reqif)
      @reqif = JSON.parse(params[:reqif]).keys_as_symbols(recursive: true)
      @result = { imported: 0, skipped: 0 }
    end

    # Kick the actual import
    def import
      build_mapping
      @reqif.each { |root| process_level(root) }
      @result
    end

    private

    attr_reader :params, :reqif, :mapping

    def fields
      @fields ||= {
        requirement_category_id: {},
        requirement_group_id: {},
        reference: {},
        title: {},
        description: {},
        status: {},
        severity: {}
      }
    end

    # The import mapping is compiled into a Hash of Hashes with
    # level (Integer) => { reference (String) => field_name (symbol) }
    def build_mapping
      @mapping = {}
      params.except(:collision_handling).each do |key, value|
        next if value == 'false'

        parts = key.split('::-::')
        @mapping[parts.first.to_i] ||= {}
        @mapping[parts.first.to_i][parts.last] = value.to_sym
      end
      raise ArgumentError, 'No import mapping selected' if @mapping.blank?
    end

    def process_level(entry)
      find_requirement(entry)
      entry[:children].each { |child| process_level(child) }
      remove_data(entry[:level])
    end

    def remove_data(target_level)
      fields.each do |_field, content|
        content.reject! { |level, _v| level == target_level }
      end
    end

    def find_requirement(entry)
      entry[:fields].each { |f| process_field(f, entry[:level]) }
      return if fields[:reference][entry[:level]].blank?

      import_requirement(entry[:level])
    end

    def import_requirement(level)
      data = fields.keys.each_with_object({}) { |field, params| params[field] = value_for(field, level) }

      if params[:collision_handling] == 'keep' && Requirement.exists?(reference: data[:reference])
        @result[:skipped] += 1
      else
        create_or_update(data)
        @result[:imported] += 1
      end
    end

    def create_or_update(data)
      req = Requirement.find_or_create_by!(reference: data[:reference])
      req.update(data)
      req.save!
    end

    # rubocop:disable Metrics/AbcSize
    def process_field(field, level)
      return if mapping.dig(level, field[:ref]).blank?

      case mapping[level][field[:ref]]
      when :category
        fields[:requirement_category_id][level] = RequirementCategory.find_or_create_by!(name: field[:value]).id
      when :group
        fields[:requirement_group_id][level] = RequirementGroup.find_or_create_by!(name: field[:value]).id
      else
        fields[mapping[level][field[:ref]]][level] = field[:value]
      end
    end
    # rubocop:enable Metrics/AbcSize

    def value_for(field, level)
      current = level.dup
      while current > -1
        return fields[field][current] if fields[field][current].present?

        current -= 1
      end
      nil
    end
  end
end
