# frozen_string_literal: true

class ReqIf
  # Reader to parse +Req.IF+ +XML+ documents with nokogiri
  #
  # Reader pics up the complete spec and fills the internal +ReqIf+
  # data model with it. Currently only plain +*.reqif+ files are supported,
  # if your files are zipped, please unzip them manually.
  #
  # Author johannes.hoelken@hawk.de
  #
  # rubocop:disable Metrics/ClassLength
  class Reader
    SIMPLE_TYPES = {
      string: 'DATATYPE-DEFINITION-STRING',
      integer: 'DATATYPE-DEFINITION-INTEGER',
      float: 'DATATYPE-DEFINITION-REAL',
      boolean: 'DATATYPE-DEFINITION-BOOLEAN',
      xhtml: 'DATATYPE-DEFINITION-XHTML',
      date: 'DATATYPE-DEFINITION-DATE'
    }.freeze

    def self.from_xml(file_path)
      reader = ReqIf::Reader.new(Nokogiri::XML(File.open(file_path)))
      reader.read
      reader.req_if
    end

    attr_reader :req_if

    def initialize(doc)
      @doc = doc
      @req_if = ReqIf.new
    end

    def read
      read_data_types
      read_object_types
      read_objects
      read_spec_types
      read_specs
    end

    private

    attr_reader :doc

    def read_specs
      doc.xpath('//xmlns:SPECIFICATION').each do |spec|
        definition = req_if.spec_types[spec.at_xpath('xmlns:TYPE/xmlns:SPECIFICATION-TYPE-REF').text]
        req_if.specifications[id_of(spec)] = {
          name: name_of(spec),
          type: definition,
          columns: read_object_values(spec, definition),
          children: read_children(spec)
        }
      end
    end

    def read_children(object)
      object.xpath('xmlns:CHILDREN/xmlns:SPEC-HIERARCHY').each_with_object({}) do |entry, children|
        object_id = entry.at_xpath('xmlns:OBJECT/xmlns:SPEC-OBJECT-REF').text
        children[id_of(entry)] = {
          ref: object_id,
          object: req_if.spec_objects[object_id],
          children: read_children(entry)
        }
      end
    end

    def read_spec_types
      doc.xpath('//xmlns:SPECIFICATION-TYPE').each do |type|
        req_if.spec_types[id_of(type)] = { name: name_of(type), columns: read_spec_attributes(type) }
      end
    end

    def read_objects
      doc.xpath('//xmlns:SPEC-OBJECT').each do |object|
        spec_type = object.at_xpath('xmlns:TYPE/xmlns:SPEC-OBJECT-TYPE-REF').text
        definitions = req_if.object_types[spec_type]
        req_if.spec_objects[id_of(object)] = {
          spec_type: spec_type,
          values: read_object_values(object, definitions)
        }
      end
    end

    def read_object_values(object, definitions)
      object.at_xpath('xmlns:VALUES').children.each_with_object([]) do |value, result|
        next if value.name == 'text'

        result <<
          if value.name == 'ATTRIBUTE-VALUE-ENUMERATION'
            enum_value(value, definitions[:columns])
          else
            simple_value(value)
          end
      end
    end

    def simple_value(value)
      {
        value: value_of(value),
        type: definition_id(value)
      }
    end

    def enum_value(value, columns)
      type = definition_id(value)
      data = []
      value.xpath('xmlns:VALUES/xmlns:ENUM-VALUE-REF').each { |ref| data << columns[type][:type][:values][ref.text] }
      {
        value: data.join(', '),
        type: type
      }
    end

    def definition_id(object)
      object.at_xpath("xmlns:DEFINITION/xmlns:#{object.name.gsub('VALUE', 'DEFINITION')}-REF").text
    end

    def read_object_types
      doc.xpath('//xmlns:SPEC-OBJECT-TYPE').each do |type|
        req_if.object_types[id_of(type)] = { name: name_of(type), columns: read_spec_attributes(type) }
      end
    end

    def read_spec_attributes(type)
      type.at_xpath('xmlns:SPEC-ATTRIBUTES').children.each_with_object({}) do |definition, result|
        next if id_of(definition).blank?

        type = definition.at_xpath("xmlns:TYPE/xmlns:#{definition.name.gsub('ATTRIBUTE', 'DATATYPE')}-REF").text
        result[id_of(definition)] = {
          name: name_of(definition),
          ref: type,
          type: req_if.data_types[type]
        }
      end
    end

    def read_data_types
      read_simple_data_types
      read_enumeration_types
    end

    def read_simple_data_types
      SIMPLE_TYPES.each do |key, value|
        doc.xpath("//xmlns:#{value}").each do |type|
          req_if.data_types[type.attribute('IDENTIFIER').to_s] = { type: key, name: type.attribute('LONG-NAME').to_s }
        end
      end
    end

    def read_enumeration_types
      doc.xpath('//xmlns:DATATYPE-DEFINITION-ENUMERATION').each do |type|
        values = {}
        type.xpath('xmlns:SPECIFIED-VALUES/xmlns:ENUM-VALUE').each { |value| values[id_of(value)] = name_of(value) }
        req_if.data_types[id_of(type)] = { type: :enum, name: name_of(type), values: values }
      end
    end

    def id_of(node)
      node.attribute('IDENTIFIER').to_s
    end

    def name_of(node)
      node.attribute('LONG-NAME').to_s
    end

    def value_of(node)
      node.attribute('THE-VALUE').to_s
    end
  end
  # rubocop:enable Metrics/ClassLength
end
