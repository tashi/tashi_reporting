# frozen_string_literal: true

# SPR model
# Author johannes.hoelken@hawk.de
class Spr < ApplicationRecord
  has_many :procedure_spr_links, dependent: :destroy
  has_many :procedures, through: :procedure_spr_links

  validates :reference, presence: true, uniqueness: true

  def used?
    procedure_spr_links.exists?
  end
end
