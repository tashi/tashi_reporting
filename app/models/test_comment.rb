# frozen_string_literal: true

# Role model
# Author johannes.hoelken@hawk.de
class TestComment < ApplicationRecord
  belongs_to :procedure_result

  validates :user, presence: true
  validates :text, presence: true
end
