# frozen_string_literal: true

# Role model
# Author johannes.hoelken@hawk.de
class Review < ApplicationRecord
  belongs_to :procedure
  belongs_to :review_state

  def as_json(options = nil)
    json = super
    json[:status] = review_state.name
    json
  end
end
