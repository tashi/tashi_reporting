# frozen_string_literal: true

# Tag model
#
# Author johannes.hoelken@hawk.de
class Tag < ApplicationRecord
  has_many :procedure_tags_links
  has_many :procedures, through: :procedure_tags_links

  validates :name, presence: true, uniqueness: true
  validates :color, presence: true
  validates_format_of :color, with: ColorScanner::COLOR_PATTERN

  # Get the matching text color for the tag
  def text_color
    ColorScanner.text_color(color)
  end

  # Translate this instance into JSON
  def as_json(options = {})
    json = super
    json['text_color'] = text_color
    json
  end

  def used?
    procedure_tags_links.exists?
  end
end
