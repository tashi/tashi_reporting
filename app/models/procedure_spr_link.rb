# frozen_string_literal: true

# Role model
# Author johannes.hoelken@hawk.de
class ProcedureSprLink < ApplicationRecord
  belongs_to :procedure
  belongs_to :spr

  validates :procedure_id, uniqueness: { scope: :spr_id, message: 'and SPR already linked.' }
end
