# frozen_string_literal: true

# Base Model to generate procedure linking information
#
# Author johannes.hoelken@hawk.de
class MatrixBase
  protected

  # Retrieve a link matrix of test procedure to requirement
  def generate_json(clazz, field)
    json = []
    clazz.left_outer_joins(:procedures).find_each do |model|
      data = { procedures: model.procedures.present? ? model.procedures.map(&:as_json) : nil }
      data[field] = model
      json << data
    end
    json.uniq
  end

  # Retrieve a matrix with coverage information per SPR.
  # Will report the results off all linked procedures within the given test run
  def generate_coverage(testrun_id, clazz, field)
    json = []
    run = ReportingTestRun.left_outer_joins(:procedure_results).find_by(id: testrun_id)
    clazz.left_outer_joins(:procedures).find_each do |model|
      data = { run: run.id, procedures: model.procedures.present? ? proc_coverage(model.procedures, run) : nil }
      data[field] = model
      json << data
    end
    json.uniq
  end

  def proc_coverage(procedures, run)
    procedures.each_with_object([]) do |proc, json|
      result = run.procedure_results.find_by(procedure_id: proc.id)
      json << proc_result(proc, result)
    end
  end

  def proc_result(proc, result)
    {
      id: proc.id,
      name: proc.name,
      result: result.blank? ? 'N/A' : result.result,
      css: result.blank? ? 'cell-NA' : "cell-#{result.result_css}"
    }
  end
end
