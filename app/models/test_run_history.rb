# frozen_string_literal: true

# Pseudo Model to build history information of a given run
# Provides insights on the executed procedures (including results), the execution duration etc.
#
# Author johannes.hoelken@hawk.de
class TestRunHistory
  attr_reader :run_id

  # Get the history of the selected test run
  # @param run_id (Integer) the DB id of the run we want to see the history for
  # @param lookback (Integer) the total number of history runs to report (including current)
  # @return (TestRunHistory)
  def self.of(run_id, lookback: 10)
    TestRunHistory.new(run_id, lookback: lookback).preload
  end

  # Constructor
  # @param run_id (Integer) the DB id of the run we want to see the history for
  # @param lookback (Integer) the total number of history runs to report (including current)
  def initialize(run_id, lookback: 10)
    @run_id = run_id
    @lookback = lookback
  end

  # Preload all information to avoid DB calls while rendering the view
  def preload
    data
    self
  end

  # Main data container that holds all the information requested
  #  - :run_ids contains an array of all DB ids of the reported runs
  #  - :runs contains an array with summary information of the runs reported
  #  - :results contains an array of all procedures executed in all runs
  #
  # @return (Hash)
  def data
    @data ||= {
      run_ids: historic_runs.map(&:id),
      runs: run_summary,
      results: results_summary
    }
  end

  # information on the current run
  def current
    @current ||= ReportingTestRun.find_by!(id: run_id)
  end

  private

  attr_reader :lookback

  def historic_runs
    @historic_runs ||= ReportingTestRun
                       .where('id <= ?', current.id)
                       .where(environment: current.environment, specification: current.specification)
                       .order(id: :desc).limit(lookback)
  end

  def results_summary
    procedures.each_with_object([]) do |procedure, results|
      results << {
        procedure: procedure,
        executions: collect_executions(procedure)
      }
    end
  end

  def procedures
    historic_runs.each_with_object([]) { |run, result| add_run_procedures(run, result) }.sort_by { |p| p[:name] }
  end

  def add_run_procedures(run, procedures)
    run.results.each do |result|
      next if procedures.any? { |p| p[:id] == result.procedure_id }

      procedures << {
        id: result.procedure.id,
        name: result.procedure.name,
        tags: result.procedure.tags.as_json
      }
    end
  end

  def collect_executions(procedure)
    historic_runs.each_with_object([]) do |run, executions|
      result = run.results.find_by(procedure_id: procedure[:id])
      next if result.blank?

      executions << {
        run: run.id,
        css: "cell-#{result.result_css}",
        result: result.result,
        summary: result.summary_data
      }
    end
  end

  def run_summary
    historic_runs.each_with_object([]) do |run, summary|
      summary << {
        id: run.id,
        name: run.name,
        duration: run.end_time.present? ? ((run.end_time - run.start_time) / 60.0).ceil(2) : 0,
        total: run.results.size
      }.merge(run.summary)
    end
  end
end
