# frozen_string_literal: true

# Role model
# Author johannes.hoelken@hawk.de
class UserRole < ApplicationRecord
  belongs_to :user
  belongs_to :role
end
