# frozen_string_literal: true

# Pseudo Model to generate requirement procedure linking information
#
# Author johannes.hoelken@hawk.de
class ReqProcMatrix < MatrixBase
  # Retrieve a link matrix of test procedure to requirement
  def as_json(_options = {})
    generate_json(Requirement, :req)
  end

  # Retrieve a matrix with coverage information per SPR.
  # Will report the results off all linked procedures within the given test run
  def coverage(testrun_id)
    generate_coverage(testrun_id, Requirement, :req)
  end
end
