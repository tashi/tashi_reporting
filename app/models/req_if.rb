# frozen_string_literal: true

# Internal +ReqIf+ Data Model
#
# Representation of a +*.reqif+ XML document
# The main content is located in the +specifications+ attribute, which
# contains all specifications of the document
#
# Author johannes.hoelken@hawk.de
class ReqIf
  require 'nokogiri'

  def self.from_xml(file_path)
    ReqIf::Reader.from_xml(file_path)
  end

  attr_accessor :data_types, :object_types, :spec_objects, :specifications, :spec_types

  def initialize
    @data_types = {}
    @object_types = {}
    @spec_objects = {}
    @specifications = {}
    @spec_types = {}
    @nesting_level = {}
    @children = {}
  end

  # Get the max nesting level of the ReqIF spec by id.
  def nesting_level(id)
    return @nesting_level[id] if @nesting_level.present?

    @specifications.each do |spec_id, spec|
      @nesting_level[spec_id] = spec_nesting_level(spec)
    end
    @nesting_level[id]
  end

  def type_refs(id, level)
    extract_type_refs(children(id)[level])
  end

  def jsonize(spec_id)
    jsonize_children(specifications[spec_id][:children])
  end

  private

  def jsonize_children(children, level = 0)
    children.each_with_object([]) do |(id, child), json|
      type = object_types[child[:object][:spec_type]]
      json << {
        id: id,
        level: level,
        type: type[:name],
        fields: jsonize_fields(child[:object][:values], type),
        children: jsonize_children(child[:children], level + 1)
      }
    end
  end

  def jsonize_fields(values, type)
    values.each_with_object([]) do |value, fields|
      fields << {
        ref: value[:type],
        name: type[:columns][value[:type]][:name],
        value: value[:value]
      }
    end
  end

  def extract_type_refs(data)
    data.each_with_object([]) { |entry, refs| refs << entry[:object][:spec_type] }
  end

  def children(id)
    return @children[id] if @children.present?

    specifications.each { |spec_id, spec| @children[spec_id] = children_of(id, spec) }
    @children[id]
  end

  def children_of(id, spec)
    children = []
    children << find_children(spec).values
    nesting_level(id).times do |level|
      current = children[level].each_with_object([]) { |child, result| result << find_children(child).values }
      children << current.flatten
    end
    children
  end

  def spec_nesting_level(object, start = 0)
    children = find_children(object)
    return start if children.blank?

    children.map { |c| spec_nesting_level(c, start + 1) }.max
  end

  def find_children(object)
    object = object.find { |e| e.is_a?(Hash) && e.keys.include?(:children) } if object.is_a?(Array)
    return [] if object.blank?

    object[:children]
  end
end
