# frozen_string_literal: true

# Role model
# Author johannes.hoelken@hawk.de
class ReviewState < ApplicationRecord
  has_many :reviews

  validates :name, presence: true, uniqueness: true

  def used?
    reviews.exists?
  end
end
