# frozen_string_literal: true

# User model
# Author johannes.hoelken@hawk.de
class User < ApplicationRecord
  has_many :user_roles, dependent: :destroy
  has_many :roles, through: :user_roles

  has_secure_password

  # Check if the user has the requested role
  # @param roles (Array) the role(s) to check
  def has?(*roles)
    roles.each do |role|
      return true if role_names.include?(role.to_s)
    end
    false
  end

  def role_names
    roles.select(:name).order(name: :asc).map(&:name)
  end

  private

  def symbolic_roles
    %i[admin tester witness user]
  end
end
