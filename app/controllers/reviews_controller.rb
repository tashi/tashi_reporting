# frozen_string_literal: true

# Controller for Test Procedure Execution Comments
#
# Author johannes.hoelken@hawk.de
class ReviewsController < ApplicationController
  before_action :require_aiv

  def index
    render json: Review.where(procedure_id: params[:procedure_id]).order(created_at: :asc)
  end

  def create
    render json: create_review
  rescue StandardError => e
    render json: "Error adding comment: #{e.message}", status: :bad_request
  end

  private

  def create_review
    Review.create!(
      review_state: ReviewState.find_by!(name: params[:state]),
      user: current_user.name,
      text: params[:text],
      procedure: Procedure.find_by!(id: params[:procedure_id])
    )
  end
end
