# frozen_string_literal: true

# Controller for interaction with Tag model
#
# Author johannes.hoelken@hawk.de
class ReviewStatesController < ApplicationController
  before_action :require_aiv

  def index
    @states = ReviewState.order(:name)
  end

  def create
    ReviewState.create!(name: params[:name], description: params[:desc])
    redirect_to '/review-states'
  rescue StandardError => e
    flash.now[:error] = "Could not create State: #{e.message}"
    @states = ReviewState.order(:name)
    render 'index'
  end

  def destroy
    destroy_state
    respond_to do |format|
      format.html { redirect_to '/review-states' }
      format.js { render js: "window.location.href='/review-states'" }
    end
  end

  private

  def destroy_state
    state = ReviewState.find_by!(id: params[:delid])
    raise "#{state.name} is in build in!" if state.build_in
    raise "#{state.name} is in use!" if state.used?

    state.destroy!
  rescue StandardError => e
    flash[:error] = "Cannot destroy: #{e.message}"
  end
end
