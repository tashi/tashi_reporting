# frozen_string_literal: true

# Controller for interaction with Tag model
#
# Author johannes.hoelken@hawk.de
class TagsController < ApplicationController
  before_action :require_aiv

  def index
    @tags = Tag.order(:name)
  end

  def create
    Tag.create!(name: params[:name], desc: params[:desc], color: params[:color])
    redirect_to '/tags'
  rescue StandardError => e
    flash.now[:error] = "Could not create Tag: #{e.message}"
    @tags = Tag.order(:name)
    render 'index'
  end

  def destroy
    destroy_tag
    respond_to do |format|
      format.html { redirect_to tags_url }
      format.js { render js: "window.location.href='#{tags_url}'" }
    end
  end

  def create_link
    raise "Cannot link tag to #{params['object']}" unless params['object'] == 'procedure'

    tag = Tag.find_by!('name ILIKE ?', params['tag'])
    ProcedureTagsLink.create!(tag: tag, procedure: Procedure.find_by!(id: params['object_id']))
    render json: tag
  rescue StandardError => e
    render json: e.message, status: :bad_request
  end

  def unlink
    ProcedureTagsLink.find_by!(tag_id: params['tag'], procedure_id: params['procedure']).destroy
    render json: {}
  rescue StandardError => e
    render json: e.message, status: :bad_request
  end

  private

  def destroy_tag
    tag = Tag.find_by!(id: params[:delid])
    raise "#{tag.name} is in use!" if tag.used?

    tag.destroy!
  rescue StandardError => e
    flash[:error] = "Cannot destroy: #{e.message}"
  end
end
