# frozen_string_literal: true

# Controller for Test Procedure Execution Comments
#
# Author johannes.hoelken@hawk.de
class CommentsController < ApplicationController
  before_action :require_tester

  def this_execution
    render json: TestComment.where(procedure_result_id: params[:result_id]).order(created_at: :asc)
  end

  def all_executions
    result = ProcedureResult.find_by!(id: params[:result_id])
    comments = TestComment.joins(:procedure_result)
                          .where('procedure_results.procedure_id = ?', result.procedure_id)
                          .order(created_at: :asc)
    render json: comments
  end

  def create
    model = TestComment.create!(user: current_user&.name, text: params[:text], procedure_result_id: params[:result_id])
    render json: model
  rescue StandardError => e
    render json: "Error adding comment: #{e.message}", status: :bad_request
  end
end
