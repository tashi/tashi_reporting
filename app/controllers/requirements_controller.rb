# frozen_string_literal: true

# Controller for Requirement related requests
#
# Author johannes.hoelken@hawk.de
# rubocop:disable Metrics/ClassLength
class RequirementsController < ApplicationController
  require 'csv'
  before_action :require_aiv

  def index
    @requirements = Requirement.left_outer_joins(:procedures, :requirement_category, :requirement_group).all
    respond_to do |format|
      format.html
      format.json { render json: RequirementsTable.new(params) }
    end
  end

  def import
    case params[:type]
    when 'ReqIF'
      @type = :reqif
      @data = ReqIf::Reader.from_xml(params[:file].tempfile)
    when 'CSV'
      @type = :csv
      @data = CSV.parse(params[:file].tempfile, headers: true, col_sep: ';')
    else
      flash[:error] = "Import format #{params[:type]} not supported."
    end
  end

  def import_reqif
    flash[:success] = "Import done: #{ReqIf::Importer.run(params)}"
    render js: "window.location.href='#{requirements_url}'"
  rescue StandardError => e
    Rails.logger.error(e.message)
    render plain: "Error importing requirements: [#{e.class.name}] #{e.message}", status: :internal_server_error
  end

  def import_csv
    flash[:success] = "Import done: #{Importer::Requirements.run(params)}"
    render js: "window.location.href='#{requirements_url}'"
  rescue StandardError => e
    Rails.logger.error(e.message)
    render plain: "Error importing requirements: [#{e.class.name}] #{e.message}", status: :internal_server_error
  end

  def edit
    @model = Requirement.left_outer_joins(:procedures, :requirement_category, :requirement_group)
                        .find_by!(id: params[:id])
                        .as_json(include: %i[procedures requirement_category requirement_group]).deep_symbolize_keys
    render 'edit', layout: false
  end

  def matrix
    render json: ReqProcMatrix.new
  end

  def coverage_matrix
    respond_to do |format|
      format.html do
        @run = TestRun.find_by(id: params[:id])
        @data = ReqProcMatrix.new.coverage(params[:id])
      end
      format.json { render json: ReqProcMatrix.new.coverage(params[:id]) }
    end
  end

  def update
    flash[:success] = "Requirement '#{update_requirement.reference}' updated."
    render js: "window.location.href='#{requirements_url}'"
  rescue StandardError => e
    render plain: e.message, status: :bad_request
  end

  def create
    flash[:success] = "Requirement '#{create_requirement.reference}' created."
    render js: "window.location.href='#{requirements_url}'"
  rescue StandardError => e
    render plain: e.message, status: :bad_request
  end

  def destroy
    destroy_requirement
    respond_to do |format|
      format.html { redirect_to requirements_url }
      format.js { render js: "window.location.href='#{requirements_url}'" }
    end
  end

  def create_link
    req = Requirement.find_by!('reference ILIKE ?', params['req'])
    proc = Procedure.find_by!('name ILIKE ?', params['proc'])
    ProcedureRequirementsLink.create!(requirement: req, procedure: proc)
    render json: params[:dir] == 'procToReq' ? proc : req
  rescue StandardError => e
    render json: e.message, status: :bad_request
  end

  def unlink
    ProcedureRequirementsLink.find_by!(requirement_id: params['req'], procedure_id: params['proc']).destroy
    render json: {}
  rescue StandardError => e
    render json: e.message, status: :bad_request
  end

  private

  def create_requirement
    Requirement.create!(
      requirement_category_id: category_id,
      requirement_group_id: group_id,
      reference: params[:reference],
      title: params[:title],
      description: params[:description],
      severity: params[:severity],
      status: params[:status]
    )
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  def update_requirement
    model = Requirement.find_by!(id: params[:id])
    model.update(
      requirement_category_id: category_id,
      requirement_group_id: group_id,
      reference: params[:reference],
      title: params[:title],
      description: params[:description],
      severity: params[:severity],
      status: params[:status]
    )
    model.save!
    model
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength

  def category_id
    RequirementCategory.find_or_create_by(name: params[:category])&.id
  end

  def group_id
    RequirementGroup.find_or_create_by(name: params[:group])&.id
  end

  def destroy_requirement
    model = Requirement.find_by!(id: params[:delid])
    msg = model.used? ? "Was linked to #{model.procedure_requirements_links.count} procedure(s)." : ''
    model.destroy!
    flash[:success] = "'#{model.reference}' deleted. #{msg}"
  rescue StandardError => e
    flash[:error] = "Cannot destroy: #{e.message}"
  end
end
# rubocop:enable Metrics/ClassLength
