# frozen_string_literal: true

# Controller for static views
class StaticController < ApplicationController
  def index
    if current_user.blank?
      render 'about'
    else
      redirect_to current_user.personal_index
    end
  end

  def about; end
end
