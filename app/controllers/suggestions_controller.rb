# frozen_string_literal: true

# Controller to create responses for the autocomplete plugin
#
# Author johannes.hoelken@hawk.de
class SuggestionsController < ApplicationController
  before_action :require_user

  # Returns data suitable for autosuggestion fields.
  def index
    render json: suggestions(params['type'], params['q'])
  end

  private

  def suggestions(type, search)
    case type
    when 'tags' then tag_suggestions(search)
    when 'sprs' then spr_suggestions(search)
    when 'procedures' then procedure_suggestions(search)
    when 'requirements' then requirements_suggestions(search)
    when 'req_category' then req_category_suggestions(search)
    when 'req_group' then req_group_suggestions(search)
    else []
    end
  end

  def tag_suggestions(search)
    Tag.where('name ILIKE ?', "%#{search}%").order(:name).limit(10).map(&:name)
  end

  def procedure_suggestions(search)
    Procedure.where('name ILIKE ?', "%#{search}%").order(:name).limit(10).map(&:name)
  end

  def spr_suggestions(search)
    Spr.where('reference ILIKE ?', "%#{search}%").order(:reference).limit(10).map(&:reference)
  end

  def requirements_suggestions(search)
    Requirement.where('reference ILIKE ?', "%#{search}%").order(:reference).limit(10).map(&:reference)
  end

  def req_category_suggestions(search)
    RequirementCategory.where('name ILIKE ?', "%#{search}%").order(:name).limit(10).map(&:name)
  end

  def req_group_suggestions(search)
    RequirementGroup.where('name ILIKE ?', "%#{search}%").order(:name).limit(10).map(&:name)
  end
end
