# frozen_string_literal: true

##
# Controller to control user sessions
# Login and logout actions
#
# Author johannes.hoelken@hawk.de
#
class SessionsController < ApplicationController
  include SessionsHelper

  # Just render the form
  def new; end

  # Create new session (login)
  def create
    target = login_user(User.find_by(name: params[:name]))
    respond_to do |format|
      format.html { redirect_to target }
      format.js   { render js: "window.location.href='#{target}'" }
    end
  end

  # destroy session (logout)
  def destroy
    destroy_session
    flash[:success] = 'You have been logged out. See you soon!'
    redirect_to login_url
  end

  private

  def login_user(user)
    Rails.logger.info user.inspect
    if user&.authenticate(params[:password])
      session[:user_id] = user.id
      root_url
    else
      sleep(0.75)
      flash[:error] = 'Either username or password incorrect.'
      login_url
    end
  end
end
