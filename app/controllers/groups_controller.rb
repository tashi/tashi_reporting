# frozen_string_literal: true

# Controller for Group related requests
#
# Author johannes.hoelken@hawk.de
class GroupsController < ApplicationController
  before_action :require_tester

  def dependencies
    render json: GroupRelation.where(test_run_id: params[:run])
  end
end
