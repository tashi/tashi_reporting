# frozen_string_literal: true

# Controller for different result types
# Read only controller
# Author johannes.hoelken@hawk.de
class ResultsController < ApplicationController
  before_action :require_tester

  def index
    respond_to do |format|
      format.html
      format.json { render json: ResultOverviewTable.new(params) }
    end
  end

  def run
    @run = ReportingTestRun.find_by!(id: params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @run.result_json }
    end
  end

  def procedure
    respond_to do |format|
      format.html { procedure_html_response }
      format.json { render json: ProcHistoryTable.new(params, procedure: params[:procedure]) }
    end
  end

  def history
    @history = TestRunHistory.of(params[:id])
  end

  def procedure_file
    entry = select_files(find_files(params[:id]), CGI.unescape(params[:path]).split('/'))
    if entry.present?
      send_file(entry[:path])
    else
      flash[:error] = "File '#{params[:path]}' not found for procedure"
    end
  end

  private

  def procedure_html_response
    procedure_html_data
    flash[:warn] = 'Could not resolve logfile. Showing only DB content...' unless @log.logfile_present?
  rescue StandardError => e
    Rails.logger.error("[#{e.class.name}] #{e.message}")
    Rails.logger.debug(e.backtrace.join("\n"))
    flash[:error] = e.message
  end

  def procedure_html_data
    @proc = ProcedureResult.find_by_run_and_name(params[:run], params[:procedure])
    @total = ProcedureHistory.find_by(id: @proc.procedure_id).total_results
    @log = ProcedureLog.parse(@proc)
  end

  def find_files(id)
    ProcedureLog.new(ProcedureResult.find_by(id: id)).read(complete: false).files
  end

  def select_files(files, path)
    entry = files.find do |f|
      f[:path].basename('.*').to_s == path.first
    end
    entry = select_files(files, path - [path.first]) if entry.present? && entry[:folder]
    entry
  end
end
