# frozen_string_literal: true

# Controller for interaction with Tag model
#
# Author johannes.hoelken@hawk.de
# rubocop:disable Metrics/ClassLength
class SprsController < ApplicationController
  require 'csv'
  before_action :require_aiv

  def import
    if params[:file]
      @data = CSV.parse(params[:file].tempfile, headers: true, col_sep: ';')
    else
      flash[:success] = "Import done: #{Importer::Sprs.run(params)}"
      render js: js_redirect_to(sprs_url)
    end
  rescue StandardError => e
    import_error_handling(e)
  end

  def spr_matrix
    render json: SprProcMatrix.new
  end

  def coverage_matrix
    respond_to do |format|
      format.html do
        @run = TestRun.find_by(id: params[:id])
        @data = SprProcMatrix.new.coverage(params[:id])
      end
      format.json { render json: SprProcMatrix.new.coverage(params[:id]) }
    end
  end

  def index
    respond_to do |format|
      format.html
      format.json { render json: SprsTable.new(params) }
    end
  end

  def edit
    @spr = Spr.left_outer_joins(:procedures).find_by!(id: params[:id]).as_json(include: :procedures).deep_symbolize_keys
    render 'edit', layout: false
  end

  def update
    flash[:success] = "SPR '#{update_spr.reference}' updated."
    render js: "window.location.href='#{sprs_url}'"
  rescue StandardError => e
    render plain: e.message, status: :bad_request
  end

  def create
    flash[:success] = "SPR '#{create_spr.reference}' created."
    render js: "window.location.href='#{sprs_url}'"
  rescue StandardError => e
    render plain: e.message, status: :bad_request
  end

  def destroy
    destroy_spr
    respond_to do |format|
      format.html { redirect_to sprs_url }
      format.js { render js: "window.location.href='#{sprs_url}'" }
    end
  end

  def create_link
    spr = Spr.find_by!('reference ILIKE ?', params['spr'])
    proc = Procedure.find_by!('name ILIKE ?', params['proc'])
    ProcedureSprLink.create!(spr: spr, procedure: proc)
    render json: params[:dir] == 'procToSpr' ? proc : spr
  rescue StandardError => e
    render json: e.message, status: :bad_request
  end

  def unlink
    ProcedureSprLink.find_by!(spr_id: params['spr'], procedure_id: params['proc']).destroy
    render json: {}
  rescue StandardError => e
    render json: e.message, status: :bad_request
  end

  private

  def create_spr
    Spr.create!(
      reference: params[:reference],
      title: params[:title],
      description: params[:description],
      severity: params[:severity]
    )
  end

  def update_spr
    spr = Spr.find_by!(id: params[:id])
    spr.update(
      reference: params[:reference],
      title: params[:title],
      description: params[:description],
      severity: params[:severity]
    )
    spr.save!
    spr
  end

  def destroy_spr
    spr = Spr.find_by!(id: params[:delid])
    msg = spr.used? ? "Was linked to #{spr.procedure_spr_links.count} procedure(s)." : ''
    spr.destroy!
    flash[:success] = "SPR '#{spr.reference}' deleted. #{msg}"
  rescue StandardError => e
    flash[:error] = "Cannot destroy: #{e.message}"
  end

  def import_error_handling(error)
    respond_to do |format|
      format.js { render plain: "Error importing SPRs: #{error.message}", status: :internal_server_error }
      format.html { flash[:error] = "Error importing SPRs: #{error.message}" and redirect_to sprs_url }
    end
  end
end
# rubocop:enable Metrics/ClassLength
