# frozen_string_literal: true

# CRUD Controller for user model
# Author johannes.hoelken@hawk.de
class UsersController < ApplicationController
  before_action :require_admin, except: %i[update profile]
  before_action :set_user, only: %i[edit update destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all.order(name: :asc)
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit; end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_url, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    update_user!
    update_roles!
    redirect_back(fallback_location: edit_user_path(@user), notice: 'User was successfully updated.')
  rescue StandardError => e
    flash[:error] = "Error while editing: #{e.message}"
    render :edit
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def profile
    @user = current_user
    render :edit
  end

  private

  def update_user!
    @user.update!(user_params)
  end

  def update_roles!
    return unless current_user.has?(:admin)

    @user.user_roles.delete_all
    params[:roles].each do |role_id|
      UserRole.create!(user: @user, role: Role.find_by!(id: role_id))
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:name, :personal_index, :password, :password_confirmation)
  end
end
