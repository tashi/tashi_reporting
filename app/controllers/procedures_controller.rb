# frozen_string_literal: true

# Controller for procedures views
# including json views for history/summary visualization
# Author johannes.hoelken@hawk.de
class ProceduresController < ApplicationController
  before_action :require_aiv

  def check_states
    result = update_states
    render json: { msg: "Updated #{result[:total]} states (#{result[:new]} new, #{result[:updated]} updated)." }
  rescue StandardError => e
    Rails.logger.error("Error updating procedure states [#{e.class.name}] #{e.message}")
    render json: "Error updating procedure states: #{e.message}", status: :internal_server_error
  end

  def index
    respond_to do |format|
      format.html
      format.json { render json: ProceduresOverviewTable.new(params) }
    end
  end

  def show
    @proc = Procedure.find_by!(name: params[:name])
    @total = ProcedureHistory.find_by(id: @proc.id).total_results
  end

  def weekly_history
    render json: ProcedureHistory.find_by(id: params[:id]).weekly_stats
  end

  def total_results
    render json: ProcedureHistory.find_by(id: params[:id]).total_results
  end

  private

  def update_states
    new = 0
    updated = 0
    Procedure.find_each do |proc|
      new += 1 if new_procedure(proc)
      updated += 1 if changed_procedure(proc)
    end
    { new: new, updated: updated, total: new + updated }
  end

  def new_procedure(proc)
    return false if proc.review_state.present?

    Review.create!(
      review_state: ReviewState.find_by!(name: 'NEW'),
      user: :system,
      text: 'New test procedure detected.',
      procedure: proc
    )
  end

  def changed_procedure(proc)
    return false if proc.reviews.order(created_at: :desc).last.created_at >= proc.updated_at

    Review.create!(
      review_state: ReviewState.find_by!(name: 'UPDATED'),
      user: :system,
      text: 'Test procedure was updated since last review.',
      procedure: proc
    )
  end
end
