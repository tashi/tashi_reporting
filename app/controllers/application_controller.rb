# frozen_string_literal: true

# Controller base class
class ApplicationController < ActionController::Base
  include SessionsHelper

  protected

  def js_redirect_to(target)
    "window.location.href='#{target}'"
  end

  def require_admin
    return if current_user&.has?(:admin)

    deny(:admin)
  end

  def require_tester
    return if current_user&.has?(:tester)

    deny(:tester)
  end

  def require_witness
    return if current_user&.has?(:witness)

    deny(:witness)
  end

  def require_user
    return if current_user&.has?(:user)

    deny(:user)
  end

  def require_aiv
    return if current_user&.has?(:tester, :witness)

    deny(:aiv)
  end

  private

  def deny(role)
    Rails.logger.warn "User #{current_user.inspect} tried to access a #{role} page."
    flash[:error] = 'Denied.'
    redirect_to '/'
  end
end
