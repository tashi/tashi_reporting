# frozen_string_literal: true

# Rails view helper for requirements views
#
# Author johannes.hoelken@hawk.de
module RequirementsHelper
  HEADLINES = %w[table-danger table-warning table-success table-info table-secondary table-active].freeze

  def reqif_children(children, types, level: 0)
    return '' if children.blank?

    html = []
    children.each do |id, child|
      html += reqif_child_info(id, child, level, types[child[:object][:spec_type]])
      html << reqif_children(child[:children], types, level: level + 1)
    end
    html.join("\n").html_safe
  end

  def reqif_child_info(id, child, level, type)
    html = []
    html << "<tr><th class='#{HEADLINES[level]}' colspan='2'> Level #{level}: #{type[:name]}</th></tr>"
    html << "<tr><th>ReqIF ID</th><td>#{id}</td></tr>"
    child[:object][:values].each do |value|
      html << "<tr><th>#{type[:columns][value[:type]][:name]}</th><td>#{value[:value]}</td></tr>"
    end
    html
  end

  def mapping_select(name)
    html = []
    html << "<select name='#{name}' class='form-control form-control-sm'>"
    html << "<option value='false' selected>Don't use on import</option>"
    html << '<option disabled>---------</option>'
    html << "<option value='reference'>Reference *</option>"
    %w[group category title description severity status].each do |field|
      html << "<option value='#{field}'>#{field.capitalize}</option>"
    end
    html << '</select>'
    html.join("\n").html_safe
  end

  def reqif_mapping_options(reqif, spec_id)
    html = []
    reqif.nesting_level(spec_id).times do |level|
      html << "<tr><th class='#{HEADLINES[level]}' colspan='3'>Level: #{level}</th></tr>"
      reqif.object_types.each do |ref, type|
        Rails.logger.info "looking for ref: #{ref}"
        next unless reqif.type_refs(spec_id, level).include?(ref)

        html += reqif_type_mapping_options(type, level)
      end
    end
    html.join("\n").html_safe
  end

  def reqif_type_mapping_options(type, level)
    html = []
    html << "<tr><th class='table-secondary' colspan='3'>Category: #{type[:name]}</th></tr>"
    type[:columns].each do |ref, column|
      html << "<tr><td>#{ref}</td><th>#{column[:name]}</th><td>#{mapping_select("#{level}::-::#{ref}")}</td></tr>"
    end
    html
  end
end
