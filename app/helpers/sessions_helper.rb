# frozen_string_literal: true

# Rails helper for session
module SessionsHelper
  # Get the current user as of session
  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    else
      @current_user = nil
    end
  end

  def destroy_session
    @current_user = nil
    session[:user_id] = nil
  end
end
