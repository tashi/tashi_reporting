# frozen_string_literal: true

# Rails view helper for SPRs views
#
# Author johannes.hoelken@hawk.de
module SprsHelper
  def spr_mapping_select(name)
    html = []
    html << "<select name='#{name}' class='form-control form-control-sm'>"
    html << "<option value='false' selected>Don't use on import</option>"
    html << '<option disabled>---------</option>'
    html << "<option value='reference'>Reference *</option>"
    %w[title description severity].each do |field|
      html << "<option value='#{field}'>#{field.capitalize}</option>"
    end
    html << '</select>'
    html.join("\n").html_safe
  end
end
