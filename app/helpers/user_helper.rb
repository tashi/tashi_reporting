# frozen_string_literal: true

# Rails view helper for user specific tasks
#
# Author johannes.hoelken@hawk.de
module UserHelper
  def index_options(user)
    options = [['User Profile', '/profile']]
    options << ['Test Results', '/results'] if user.has?(:tester)
    options << ['Test Procedures', '/procedures'] if user.has?(:tester, :witness)
    options << %w[Requirements /requirements] if user.has?(:tester, :witness)
    options << %w[Users /users] if user.has?(:admin)
    options
  end
end
