# frozen_string_literal: true

# Helper for formatting result data
# Author johannes.hoelken@hawk.de
module ResultHelper
  PASSED_STATES = [Tashi::Result::PASSED, Tashi::Result::OBSERVATIONS].freeze

  def duration(start, stop)
    return 'Unknown' if start.blank? || stop.blank?

    duration = []
    ActiveSupport::Duration.build((stop - start).abs.to_i).parts.each do |k, v|
      duration << "#{v} #{k}"
    end
    duration.join(' ')
  end

  def result_colors
    {
      Tashi::Result::PASSED => '#00d500',
      Tashi::Result::OBSERVATIONS => '#d0a000',
      Tashi::Result::FAILED => '#dd0000',
      Tashi::Result::NOT_EXECUTED => '#d5d5d5'
    }
  end

  def summary_format(result)
    html = []
    data = result.respond_to?(:summary_data) ? result.summary_data : result
    data.each do |step, msg|
      step_name = CGI.escapeHTML(step)
      html << "[Step: <strong><a href='##{step_name}'>#{step_name}</a></strong>]"
      (msg['observations'] + msg['failures']).each do |entry|
        html << CGI.escapeHTML(entry)
      end
    end
    html.join('<br />').html_safe
  end

  def summary_tooltip(procedure, run, result)
    html = "<a href=\"/results/#{run}/#{procedure}\" class=\"btn btn-sm btn-block btn-default\" type='button' "
    if result.present?
      html += "data-toggle=\"tooltip\" data-html=\"true\" title=\"#{summary_format(result)}\">"
      html += '<em class="fa fa-exclamation-triangle"></em>'
    else
      html += '><em class="fa fa-info"></em>'
    end
    html += '</a>'
    html.html_safe
  end

  def gen_download_link(log, entry, subfolder = nil)
    entry[:folder] ? folder_list(log, entry, subfolder) : file_link(log, entry, subfolder)
  end

  def file_link(log, file, subfolder = nil)
    href = "/execution/#{log.id}"
    href += "/#{subfolder}" if subfolder.present?
    href += "/#{CGI.escape(file[:path].basename.to_s)}"
    "<li><a href='#{href}' title='Download: #{file[:name]}'>#{file[:name]}</a></li>".html_safe
  end

  def folder_list(log, folder, subfolder = nil)
    html = "<li>#{folder[:name]}<ul>"
    subfolder = subfolder.present? ? File.join(subfolder, folder[:name]) : folder[:name]
    folder[:children].each { |entry| html += gen_download_link(log, entry, subfolder) }
    html += '</ul></li>'
    html.html_safe
  end

  def coverage_percent(procedures)
    return '0.0 %' if procedures.blank?

    executed = procedures.reject { |p| p[:result] == 'N/A' }.size
    "#{executed.percent_of(procedures.size)} %"
  end

  def success_percent(procedures)
    return '0.0 %' if procedures.blank?

    passed = procedures.select { |p| PASSED_STATES.include?(p[:result]) }.size
    "#{passed.percent_of(procedures.size)} %"
  end
end
