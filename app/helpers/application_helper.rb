# frozen_string_literal: true

# Base application helper
module ApplicationHelper
  # Generate breadcrumb menu from request's fullpath
  # @param path (String) the current request.fullpath, e.g. /foo/bar
  # @return (String) HTML safe string with list elements
  #
  # rubocop:disable Metrics/MethodLength
  def breadcrumbs(path)
    data = path.split('/')
    parts = ['<li class="breadcrumb-item"><a href="/"><em class="fa fa-home"></em></a></li>']
    data.each_with_index do |part, idx|
      next if part.blank?

      parts <<
        if data.last == part
          "<li class='breadcrumb-item active' aria-current='page'>#{part.capitalize}</li>"
        else
          "<li class='breadcrumb-item'><a href='#{data[0..idx].join('/')}'>#{part.capitalize}</a></li>"
        end
    end
    parts.join("\n").html_safe
  end
  # rubocop:enable Metrics/MethodLength

  # Returns the matching bootstrap alert css-class name for the given flash type
  # @param (String) type the flash type
  # @return (String) bootstrap alert css-class, e.g. 'alert-warning'
  def flash_bs_class(type)
    case type
    when 'success', 'ok' then 'alert-success'
    when 'error' then 'alert-danger'
    when 'alert', 'warn' then 'alert-warning'
    when 'notice', 'info', 'note' then 'alert-info'
    else 'alert-secondary'
    end
  end

  # Returns the matching fontawesome css-class name for the given flash type
  # @param (String) type the flash type
  # @return (String) fontawesome css-class, e.g. 'fa fa-check-circle'
  def flash_fa_class(type)
    case type
    when 'success', 'ok' then 'fa fa-check-circle'
    when 'error' then 'fa fa-times-circle'
    when 'alert', 'warn' then 'fa fa-exclamation-circle'
    when 'notice', 'info', 'note' then 'fa fa-info-circle'
    else 'fa fa-comment'
    end
  end

  # Determines the matching foreground text color by the brightness of the given
  # hex string.
  # @param (String) background the hex code, e.g. #123456
  # @return (String) the matching text-color hex code #000 or #fff
  def text_color(background)
    ColorScanner.text_color(background)
  end
end
