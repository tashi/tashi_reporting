# frozen_string_literal: true

# Helper to generate the main menu
module MenuHelper
  # Data Item for menu objects
  class MenuItem
    attr_reader :text, :icon, :link, :children, :roles

    def initialize(text:, icon:, link: '#', children: nil, roles: nil)
      @text = text
      @icon = icon
      @link = link
      @children = children
      @roles = roles
    end

    # Replace user specific content in link text
    def link_text(user)
      return text if user.blank?

      text.gsub('$$user_name$$', user.name)
    end

    # @return (String) the css icon classes of fontawesome
    def icon_class
      "fa fa-#{icon}"
    end

    def active?(path)
      (link.present? && path.start_with?(link)) || children&.any? { |c| c.active?(path) }
    end
  end

  # Content of the admin menu
  ADMIN_ITEMS = [
    MenuItem.new(text: 'Users', icon: 'user', link: '/users', roles: [:admin]),
    MenuItem.new(text: 'Settings', icon: 'cog', link: '#', roles: [:admin])
  ].freeze

  # Content of the list menu
  LIST_ITEMS = [
    MenuItem.new(text: 'Tags', icon: 'tags', link: '/tags', roles: %i[tester witness]),
    MenuItem.new(text: 'Review States', icon: 'comment-alt', link: '/review-states', roles: %i[tester witness])
  ].freeze

  # Content of the main menu
  MAIN_MENU = [
    MenuItem.new(text: 'Test Results', icon: 'chart-pie', link: '/results', roles: [:tester]),
    MenuItem.new(text: 'Test Procedures', icon: 'rocket', link: '/procedures', roles: %i[tester witness]),
    MenuItem.new(text: 'Requirements', icon: 'space-shuttle', link: '/requirements', roles: %i[tester witness]),
    MenuItem.new(text: 'Bugs/SPRs', icon: 'bug', link: '/sprs', roles: %i[tester witness]),
    MenuItem.new(text: 'List Items', icon: 'bars', children: LIST_ITEMS, roles: %i[tester witness])
  ].freeze

  # Content of the user (right) menu
  USER_MENU = [
    MenuItem.new(text: '', icon: 'wrench', children: ADMIN_ITEMS, roles: [:admin]),
    MenuItem.new(text: '$$user_name$$', icon: 'user-astronaut', link: '/profile', roles: [:user]),
    MenuItem.new(text: 'Login', icon: 'door-open', link: '/login'),
    MenuItem.new(text: 'Logout', icon: 'door-closed', link: '/logout', roles: [:user])
  ].freeze

  # Generate +html_safe+ menu content for the main menu
  def main_menu(user)
    gen_menu(MAIN_MENU, user).join("\n").html_safe
  end

  # Generate +html_safe+ menu content for the user menu
  def user_menu(user)
    gen_menu(USER_MENU, user).join("\n").html_safe
  end

  private

  def gen_menu(items, user)
    items.each_with_object([]) do |item, result|
      next unless accessible?(user, item)

      result <<
        if item.children.blank?
          list_entry(item, user)
        else
          dropdown(item, user)
        end
    end
  end

  def list_entry(item, user)
    active = item.active?(request.fullpath) ? 'active' : ''
    "<li class='nav-item #{active}'>#{gen_link(item, user)}</li>"
  end

  def dropdown(item, user, id: item.text.gsub(' ', '_').camelize)
    html = dropdown_start(item, user, id: id)
    item.children.each do |child|
      next unless accessible?(user, item)

      html << gen_link(child, user, css: 'dropdown-item')
    end
    dropdown_end(html)
  end

  def dropdown_start(item, user, id: item.text.gsub(' ', '_').camelize)
    active = item.active?(request.fullpath) ? 'active' : ''
    html = ["<li class='nav-item dropdown #{active}'>"]
    html << "<a class='nav-link dropdown-toggle' data-toggle='dropdown' href='#' id='#{id}' aria-expanded='true'>"
    html << "<em class='#{item.icon_class}'></em> #{item.link_text(user)} <span class='caret'></span></a>"
    html << "<div class=dropdown-menu aria-labelledby='#{id}'>"
  end

  def dropdown_end(html)
    html << '</div></li>'
    html.join("\n")
  end

  def gen_link(item, user, css: 'nav-link')
    active = item.active?(request.fullpath) ? 'active' : ''
    "<a class='#{css} #{active}' href='#{item.link}'><em class='#{item.icon_class}'></em> #{item.link_text(user)}</a>"
  end

  def accessible?(user, item)
    return item.roles.blank? if user.nil?
    return false if item.roles.blank?

    item.roles.any? { |role| user.has?(role) }
  end
end
