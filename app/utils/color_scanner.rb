# frozen_string_literal: true

# Utility class to scan colors represented by Hex codes, as +#AA12F4+.
#
# Author johannes.hoelken@hawk.de
class ColorScanner
  # RegEx pattern to match valid color hex codes
  COLOR_PATTERN = /\A#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})\Z/i.freeze

  # Determines the matching foreground text color by the brightness of the given
  # hex string.
  # @param (String) background the hex code, e.g. #123456
  # @return (String) the matching text-color hex code #000 or #fff
  def self.text_color(background)
    ColorScanner.new.foreground(background)
  end

  # Determines the matching foreground color by the brightness of the given
  # hex string.
  # @param (String) background the hex code, e.g. #123456
  # @return (String) the matching text-color hex code #000 or #fff
  def foreground(background)
    luminance_of(background) > 135 ? '#000' : '#fff'
  end

  # Determines the luminance of the given hex string.
  # @param (String) color the hex code, e.g. #123456
  # @return (Integer) the luminance from 0 (dark) to 255 (bright)
  def luminance_of(color)
    check_color_code(color)
    r, g, b = split_hex(color)
    ((hex_to_int(r) * 299) + (hex_to_int(g) * 587) + (hex_to_int(b) * 114)) / 1000
  end

  private

  def split_hex(color)
    m = normalize_code(color)[1..].match(/(\S{2})(\S{2})(\S{2})/)
    return [m[1], m[2], m[3]] if m.present?

    %w[00 00 00]
  end

  def hex_to_int(hex)
    "0x#{hex}".to_i(16)
  end

  def check_color_code(hex)
    raise 'Not a valid hex color code' unless hex =~ ColorScanner::COLOR_PATTERN
  end

  def normalize_code(hex)
    return hex if hex.size == 7

    p = hex.split(//)
    "#{p[0]}#{p[1]}#{p[1]}#{p[2]}#{p[2]}#{p[3]}#{p[3]}"
  end
end
