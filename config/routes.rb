# frozen_string_literal: true

# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
# rubocop:disable Metrics/BlockLength
Rails.application.routes.draw do
  get '/profile', to: 'users#profile'
  get '/users/new', to: 'users#new'
  get '/users/:id', to: 'users#edit'
  resources :users

  get '/results', to: 'results#index'
  get '/results/:id', to: 'results#run'
  get '/results/:id/spr_coverage', to: 'sprs#coverage_matrix'
  get '/results/:id/req_coverage', to: 'requirements#coverage_matrix'
  get '/results/:id/history', to: 'results#history'
  get '/results/:run/:procedure', to: 'results#procedure'
  get '/execution/:id/:path', to: 'results#procedure_file'

  get '/groups/dependencies/:run', to: 'groups#dependencies'

  get '/procedures', to: 'procedures#index'
  get '/procedures/check_states', to: 'procedures#check_states'
  get '/procedures/:name', to: 'procedures#show'
  get '/procedures/:id/weekly_history', to: 'procedures#weekly_history'
  get '/procedures/:id/total_results', to: 'procedures#total_results'

  get '/about', to: 'static#about'

  get '/tags', to: 'tags#index'
  post '/tags', to: 'tags#create'
  post '/tags/delete', to: 'tags#destroy'
  post '/tags/create_link', to: 'tags#create_link'
  get '/tags/unlink', to: 'tags#unlink'

  get '/sprs', to: 'sprs#index'
  get '/sprs/matrix', to: 'sprs#spr_matrix'
  get '/sprs/:id/edit', to: 'sprs#edit'
  post '/sprs/:id/edit', to: 'sprs#update'
  post '/sprs', to: 'sprs#create'
  post '/sprs/import', to: 'sprs#import'
  get '/sprs/delete', to: 'sprs#destroy'
  get '/sprs/link', to: 'sprs#create_link'
  get '/sprs/unlink', to: 'sprs#unlink'

  get '/requirements', to: 'requirements#index'
  get '/requirements/matrix', to: 'requirements#matrix'
  post '/requirements/import', to: 'requirements#import'
  post '/requirements/import/reqif', to: 'requirements#import_reqif'
  post '/requirements/import/csv', to: 'requirements#import_csv'
  get '/requirements/:id/edit', to: 'requirements#edit'
  post '/requirements/:id/edit', to: 'requirements#update'
  post '/requirements', to: 'requirements#create'
  get '/requirements/delete', to: 'requirements#destroy'
  get '/requirements/link', to: 'requirements#create_link'
  get '/requirements/unlink', to: 'requirements#unlink'

  post '/comments', to: 'comments#create'
  get '/comments/:result_id', to: 'comments#this_execution'
  get '/comments/:result_id/all', to: 'comments#all_executions'

  get '/review-states', to: 'review_states#index'
  post '/review-states', to: 'review_states#create'
  post '/review-states/destroy', to: 'review_states#destroy'

  get '/reviews/:procedure_id', to: 'reviews#index'
  post '/reviews', to: 'reviews#create'

  get '/suggest/:type', to: 'suggestions#index'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'

  # Default page to display on root ('/').
  root to: 'static#index'
end
# rubocop:enable Metrics/BlockLength
