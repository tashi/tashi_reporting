# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# Namespace
module TashiReporting
  # Rails application root class
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.autoload_paths << File.join(Rails.root, 'app', 'datatables')
    config.autoload_paths << File.join(Rails.root, 'app', 'utils')

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Load TASHI core migrations
    spec = Gem::Specification.find_by_name 'tashi'
    config.paths['db/migrate'] << "#{spec.gem_dir}/lib/tashi/db/migrate"
  end
end
