# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_27_085647) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "group_relations", id: false, force: :cascade do |t|
    t.integer "parent_id", null: false
    t.integer "child_id"
    t.integer "test_run_id"
  end

  create_table "procedure_requirements_links", force: :cascade do |t|
    t.integer "procedure_id", null: false
    t.integer "requirement_id", null: false
  end

  create_table "procedure_results", force: :cascade do |t|
    t.integer "test_run_id", null: false
    t.integer "procedure_id", null: false
    t.integer "run_group_id", null: false
    t.string "result", null: false
    t.string "message"
    t.datetime "start_time", null: false
    t.datetime "end_time", null: false
  end

  create_table "procedure_spr_links", force: :cascade do |t|
    t.integer "procedure_id", null: false
    t.integer "spr_id", null: false
  end

  create_table "procedure_steps", force: :cascade do |t|
    t.integer "procedure_id", null: false
    t.string "name", null: false
    t.string "instruction", null: false
    t.string "expectation", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "procedure_tags_links", force: :cascade do |t|
    t.integer "tag_id", null: false
    t.integer "procedure_id", null: false
  end

  create_table "procedures", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.string "pass_fail"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "requirement_categories", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "requirement_groups", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "requirements", force: :cascade do |t|
    t.integer "requirement_category_id"
    t.integer "requirement_group_id"
    t.string "reference", null: false
    t.string "title"
    t.string "description"
    t.string "status"
    t.string "severity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "review_states", force: :cascade do |t|
    t.string "name", null: false
    t.string "description"
    t.boolean "build_in", default: false
  end

  create_table "reviews", force: :cascade do |t|
    t.integer "review_state_id"
    t.integer "procedure_id"
    t.string "user", null: false
    t.string "text", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name", null: false
    t.string "description"
  end

  create_table "run_groups", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "sprs", force: :cascade do |t|
    t.string "reference", null: false
    t.string "title"
    t.string "description"
    t.string "severity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string "name", null: false
    t.string "desc"
    t.string "color"
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "test_comments", force: :cascade do |t|
    t.integer "procedure_result_id", null: false
    t.string "user", null: false
    t.string "text", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "test_runs", force: :cascade do |t|
    t.string "environment", null: false
    t.string "specification", null: false
    t.string "name"
    t.datetime "start_time", null: false
    t.datetime "end_time"
    t.string "results_dir"
  end

  create_table "user_roles", force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "personal_index", default: "/results"
    t.index ["name"], name: "index_users_on_name", unique: true
  end

  add_foreign_key "procedure_requirements_links", "procedures"
  add_foreign_key "procedure_requirements_links", "requirements"
  add_foreign_key "procedure_results", "procedures"
  add_foreign_key "procedure_results", "test_runs"
  add_foreign_key "procedure_spr_links", "procedures"
  add_foreign_key "procedure_spr_links", "sprs"
  add_foreign_key "procedure_steps", "procedures"
  add_foreign_key "procedure_tags_links", "procedures"
  add_foreign_key "procedure_tags_links", "tags"
  add_foreign_key "reviews", "procedures"
  add_foreign_key "reviews", "review_states"
  add_foreign_key "test_comments", "procedure_results"
  add_foreign_key "user_roles", "roles"
  add_foreign_key "user_roles", "users"
end
