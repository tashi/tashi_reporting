class AlterUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :personal_index, :string, default: '/profile'
  end
end
