class AddSprsTable < ActiveRecord::Migration[6.0]
    def change
    create_table :sprs do |t|
      t.string :reference, null: false
      t.string :title
      t.string :description
      t.string :severity
      t.timestamps
    end

    create_table :procedure_spr_links do |t|
      t.integer :procedure_id, null: false
      t.integer :spr_id, null: false
    end

    add_foreign_key :procedure_spr_links, :sprs
    add_foreign_key :procedure_spr_links, :procedures
  end
end
