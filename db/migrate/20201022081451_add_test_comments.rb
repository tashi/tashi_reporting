class AddTestComments < ActiveRecord::Migration[6.0]
  def change
    create_table :test_comments do |t|
      t.integer :procedure_result_id, null: false
      t.string :user, null: false
      t.string :text, null: false
      t.timestamps
    end

    add_foreign_key :test_comments, :procedure_results
  end
end
