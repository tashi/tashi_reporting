class ProcedureTagsLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :procedure_tags_links do |t|
      t.integer :tag_id, null: false
      t.integer :procedure_id, null: false
    end

    add_foreign_key :procedure_tags_links, :tags
    add_foreign_key :procedure_tags_links, :procedures
  end
end
