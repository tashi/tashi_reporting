class AddTags < ActiveRecord::Migration[6.0]
  def change
    create_table :tags do |t|
      t.string :name, null: false
      t.string :desc
      t.string :color
    end
    add_index :tags, :name, unique: true
  end
end
