class AddUserRolesTable < ActiveRecord::Migration[6.0]

  def change
    create_table :roles do |t|
      t.string :name, null: false
      t.string :description
    end

    create_table :user_roles do |t|
      t.integer :user_id
      t.integer :role_id
    end

    add_foreign_key :user_roles, :users
    add_foreign_key :user_roles, :roles
  end
end
