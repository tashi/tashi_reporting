class AddReviews < ActiveRecord::Migration[6.0]

  def change
    create_table :review_states do |t|
      t.string :name, null: false
      t.string :description
      t.boolean :build_in, default: false
    end

    create_table :reviews do |t|
      t.integer :review_state_id
      t.integer :procedure_id
      t.string :user, null: false
      t.string :text, null: false
      t.timestamps
    end

    add_foreign_key :reviews, :review_states
    add_foreign_key :reviews, :procedures
  end
end
