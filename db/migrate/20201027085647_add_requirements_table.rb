class AddRequirementsTable < ActiveRecord::Migration[6.0]
  def change
    create_table :requirements do |t|
      t.integer :requirement_category_id
      t.integer :requirement_group_id
      t.string :reference, null: false
      t.string :title
      t.string :description
      t.string :status
      t.string :severity
      t.timestamps
    end

    create_table :requirement_categories do |t|
      t.string :name, null: false
    end

    create_table :requirement_groups do |t|
      t.string :name, null: false
    end

    create_table :procedure_requirements_links do |t|
      t.integer :procedure_id, null: false
      t.integer :requirement_id, null: false
    end

    add_foreign_key :procedure_requirements_links, :requirements
    add_foreign_key :procedure_requirements_links, :procedures
  end
end

