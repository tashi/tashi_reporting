# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#

ROLES = [
  {name: 'user', description: 'Basic role of an authenticated user.'},
  {name: 'admin', description: 'The admin role.'},
  {name: 'tester', description: 'A member of the test development team.'},
  {name: 'witness', description: 'A stakeholder (Customer, Software verification Engineer, Product Assurance, etc.).'}
].freeze

DEFAULT_STATES = [
  {name: 'NEW', description: 'Newly added procedure', build_in: true},
  {name: 'UPDATED', description: 'Procedure was updated since the last review', build_in: true},
  {name: 'WIP', description: 'Procedure is Work in Progress.'},
  {name: 'INCOMPLETE', description: 'Procedure is incomplete or has issues'},
  {name: 'REQUEST', description: 'There are ambiguities concerning this procedure.'},
  {name: 'AGREED', description: 'Procedure is agreed with/by customer/stakeholder.'}
].freeze

DEFAULT_STATES.each { |params| ReviewState.create!(params) unless ReviewState.exists?(name: params[:name]) }
puts "review states loaded"

ROLES.each { |params| Role.create!(params) unless Role.exists?(name: params[:name]) }
puts "Roles loaded"

admin = User.create_with(password: 'StartMeUp!', password_confirmation: 'StartMeUp!').find_or_create_by!(name: 'admin')
ROLES.each do |p|
  role = Role.find_by!(name: p[:name])
  next if UserRole.exists?(user: admin, role: role)

  UserRole.create!(user: admin, role: role)
end
puts "admin user reset"
