# frozen_string_literal: true

require 'test_helper'

class ApplicationHelperTest < ActiveSupport::TestCase
  class AppHelperTestImpl
    include ApplicationHelper
  end

  setup do
    @helper = AppHelperTestImpl.new
  end

  test 'flash bootstrap class' do
    assert_equal('alert-success', @helper.flash_bs_class('success'))
    assert_equal('alert-success', @helper.flash_bs_class('ok'))

    assert_equal('alert-danger', @helper.flash_bs_class('error'))

    assert_equal('alert-warning', @helper.flash_bs_class('alert'))
    assert_equal('alert-warning', @helper.flash_bs_class('warn'))

    assert_equal('alert-info', @helper.flash_bs_class('info'))
    assert_equal('alert-info', @helper.flash_bs_class('notice'))
    assert_equal('alert-info', @helper.flash_bs_class('note'))

    assert_equal('alert-secondary', @helper.flash_bs_class('foobar'))
  end

  test 'flash fontawesome class' do
    assert_equal('fa fa-check-circle', @helper.flash_fa_class('success'))
    assert_equal('fa fa-check-circle', @helper.flash_fa_class('ok'))

    assert_equal('fa fa-times-circle', @helper.flash_fa_class('error'))

    assert_equal('fa fa-exclamation-circle', @helper.flash_fa_class('alert'))
    assert_equal('fa fa-exclamation-circle', @helper.flash_fa_class('warn'))

    assert_equal('fa fa-info-circle', @helper.flash_fa_class('info'))
    assert_equal('fa fa-info-circle', @helper.flash_fa_class('notice'))
    assert_equal('fa fa-info-circle', @helper.flash_fa_class('note'))

    assert_equal('fa fa-comment', @helper.flash_fa_class('foobar'))
  end

  test 'breadcrumb generation' do
    html = @helper.breadcrumbs('foo/bar/baz')

    assert(html.html_safe?)
    assert(html.lines[0].include?('<a href="/"><em class="fa fa-home"></em></a>'))
    assert(html.lines[1].include?('<a href=\'foo\'>Foo</a>'))
    assert(html.lines[2].include?('<a href=\'foo/bar\'>Bar</a>'))
    assert(html.lines[3].include?('aria-current=\'page\'>Baz'))
  end

  test 'should determine matching foreground-color' do
    assert_equal('#fff', @helper.text_color('#000'))
    assert_equal('#fff', @helper.text_color('#00f'))
    assert_equal('#fff', @helper.text_color('#F00'))
    assert_equal('#fff', @helper.text_color('#F0F'))
    assert_equal('#000', @helper.text_color('#0F0'))
    assert_equal('#000', @helper.text_color('#0FF'))
    assert_equal('#000', @helper.text_color('#FF0'))
    assert_equal('#000', @helper.text_color('#FFF'))
  end
end
