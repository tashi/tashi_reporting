# frozen_string_literal: true

require 'test_helper'

class ResultHelperTest < ActiveSupport::TestCase
  class ResultHelperTestImpl
    include ResultHelper
  end

  setup do
    @helper = ResultHelperTestImpl.new
  end

  test 'duration computation' do
    assert_equal('2 minutes 59 seconds', @helper.duration(3.minutes.ago, Time.now - 1))
    assert_equal('1 minutes', @helper.duration(3.minutes.ago, 2.minutes.ago))
    assert_equal('57 minutes', @helper.duration(1.hour.ago, 3.minutes.ago))
    assert_equal('6 days 23 hours 58 minutes 55 seconds', @helper.duration(1.week.ago, Time.now - 65))
  end

  test 'result_colors' do
    assert_equal(4, @helper.result_colors.size)
  end

  test 'summary_format html generator' do
    summary = {
      'test_step>1' => {
        'observations' => ['Observe me once', 'Observe me twice'],
        'failures' => ['shame on <span>you</span>', 'shame on <b>me</b>']
      }
    }
    result = Minitest::Mock.new
    result.expect :summary_data, summary

    html = @helper.summary_format(result)

    assert(html.html_safe?)
    assert(html.include?('test_step&gt;1'))
    assert(html.include?('Observe me once'))
    assert(html.include?('Observe me twice'))
    assert(html.include?('shame on &lt;span&gt;you&lt;/span&gt;'))
    assert(html.include?('shame on &lt;b&gt;me&lt;/b&gt;'))
    result.verify
  end

  test 'summary tooltip generator' do
    summary = {
      'test_step>1' => {
        'observations' => ['Observe me once', 'Observe me twice'],
        'failures' => ['shame on <span>you</span>', 'shame on <b>me</b>']
      }
    }

    html = @helper.summary_tooltip('test', 1, summary)

    assert(html.html_safe?)
    assert(html.include?('test_step&gt;1'))
    assert(html.include?('Observe me once'))
    assert(html.include?('Observe me twice'))
    assert(html.include?('shame on &lt;span&gt;you&lt;/span&gt;'))
    assert(html.include?('shame on &lt;b&gt;me&lt;/b&gt;'))
    assert(html.include?('data-toggle="tooltip"'))
  end

  test 'download link HTML generator plain file' do
    log = Minitest::Mock.new
    log.expect :id, 42

    html = @helper.gen_download_link(log, gen_entries('/path/to/foo.txt'))

    assert_equal("<li><a href='/execution/42/foo.txt' title='Download: foo.txt'>foo.txt</a></li>", html)
    assert(html.html_safe?)
    log.verify
  end

  test 'download link HTML generator sub folder' do
    log = Minitest::Mock.new
    2.times { log.expect :id, 42 }
    entry = gen_entries('/path/to/subfolder', children: %w[/path/to/subfolder/az.rb /path/to/subfolder/x.js])
    expectations = [
      '<li>subfolder<ul>',
      "<a href='/execution/42/subfolder/az.rb' title='Download: az.rb'>az.rb</a></li>",
      "<a href='/execution/42/subfolder/x.js' title='Download: x.js'>x.js</a>"
    ]

    html = @helper.gen_download_link(log, entry)

    expectations.each { |exp| assert(html.include?(exp), "Unable to find [#{exp}] in: #{html}") }
    assert(html.html_safe?)
    log.verify
  end

  private

  def gen_entries(path, children: nil)
    pathname = Pathname(path)
    {
      path: pathname,
      name: pathname.basename,
      folder: children.present?,
      children: children&.each_with_object([]) { |e, r| r << gen_entries(e) }
    }
  end
end
