# frozen_string_literal: true

require 'test_helper'

class RequirementsHelperTest < ActiveSupport::TestCase
  class RequirementsHelperTestImpl
    include RequirementsHelper
  end

  setup do
    @helper = RequirementsHelperTestImpl.new
  end

  test 'should generate Req.IF child info' do
    html = @helper.reqif_child_info(
      'asd-123',
      { object: { values: [{ type: 'abc', value: 'Data' }] } },
      99,
      { name: 'TestType', columns: { 'abc' => { name: 'Col' } } }
    ).join
    assert html.include?('Level 99: TestType')
    assert html.include?('ReqIF ID</th><td>asd-123')
    assert html.include?('Col</th><td>Data')
  end

  test 'should generate select menu' do
    html = @helper.mapping_select('foo')
    assert html.include?("name='foo'")
    %w[false group category title description severity status].each do |field|
      assert html.include?("value='#{field}'")
    end
  end

  test 'should generate mapping options' do
    html = @helper.reqif_type_mapping_options({ name: 'Test', columns: { ref: { name: 'column' } } }, 2).join
    assert html.include?("name='2::-::ref'")
    assert html.include?('Category: Test')
  end
end
