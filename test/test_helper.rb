# frozen_string_literal: true

ENV['RAILS_ENV'] = 'test'

require 'simplecov'

SimpleCov.start 'rails' do
  enable_coverage :branch

  # folders to create special groups for
  # Rails default folders are already known
  add_group 'Datatables', '/app/datatables/'

  # folders to ignore
  add_filter '/test/'
  add_filter '/db/'

  add_filter '/app/channels/'
  add_filter '/app/jobs/'
  add_filter '/app/mailers/'
end

require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/mock'
require 'json'

module ActiveSupport
  class TestCase
    # Run tests in parallel with specified workers
    parallelize(workers: :number_of_processors) if ENV['PARALLEL_TESTING']

    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    fixtures :all

    # Add more helper methods to be used by all tests here...

    def resource_path(name)
      File.join(File.dirname(__FILE__), 'resources', name)
    end

    def login(user: users(:one), password: 'secret')
      post login_url, params: { name: user.name, password: password }
      assert_redirected_to root_url
    end

    def json_response
      @json_response ||= ::JSON.parse(response.body)
    end

    def procedure
      @procedure ||= Procedure.create!(
        name: 'ProcForTesting',
        description: 'Proc Description',
        pass_fail: 'Pass/Fail criteria'
      )
    end

    def create_result(result, params = {})
      default_params = {
        test_run: test_runs(:one),
        run_group: run_groups(:one),
        start_time: 25.minutes.ago,
        end_time: 22.minutes.ago,
        procedure: procedure,
        result: result,
        message: ''
      }
      ProcedureResult.create!(default_params.merge(params))
    end

    def dt_params(sort_col: 'id', filter: nil, page_length: nil, start_page: 0)
      {
        columns: { '0' => { 'name' => sort_col }, '1' => { 'data' => sort_col } },
        order: { '0' => { column: %w[0 1].sample, dir: %w[desc asc].sample } },
        start: start_page,
        search: { value: filter },
        length: page_length,
        format: :json
      }
    end
  end
end
