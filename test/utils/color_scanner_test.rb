# frozen_string_literal: true

require 'test_helper'

class ColorScannerTest < ActiveSupport::TestCase
  setup do
    @scanner = ColorScanner.new
  end

  test 'should determine luminance of short hexes' do
    assert_equal(0, @scanner.luminance_of('#000'))
    assert_equal(76, @scanner.luminance_of('#F00'))
    assert_equal(149, @scanner.luminance_of('#0F0'))
    assert_equal(29, @scanner.luminance_of('#00f'))
    assert_equal(255, @scanner.luminance_of('#FFF'))
    assert_equal(105, @scanner.luminance_of('#F0F'))
    assert_equal(225, @scanner.luminance_of('#FF0'))
    assert_equal(178, @scanner.luminance_of('#0FF'))
    assert_equal(255, @scanner.luminance_of('#fff'))
  end

  test 'should determine luminance of full spec hexes' do
    assert_equal(0, @scanner.luminance_of('#000000'))
    assert_equal(76, @scanner.luminance_of('#FF0000'))
    assert_equal(149, @scanner.luminance_of('#00FF00'))
    assert_equal(29, @scanner.luminance_of('#0000FF'))
    assert_equal(105, @scanner.luminance_of('#FF00FF'))
    assert_equal(225, @scanner.luminance_of('#FFFF00'))
    assert_equal(178, @scanner.luminance_of('#00ffff'))
    assert_equal(255, @scanner.luminance_of('#FFFFFF'))
  end

  test 'should determine matching foreground-color' do
    assert_equal('#fff', @scanner.foreground('#000'))
    assert_equal('#fff', @scanner.foreground('#00f'))
    assert_equal('#fff', @scanner.foreground('#F00'))
    assert_equal('#fff', @scanner.foreground('#F0F'))
    assert_equal('#000', @scanner.foreground('#0F0'))
    assert_equal('#000', @scanner.foreground('#0FF'))
    assert_equal('#000', @scanner.foreground('#FF0'))
    assert_equal('#000', @scanner.foreground('#FFF'))
  end

  test 'should reject illegal hex codes' do
    assert_raises { @scanner.luminance_of('#00') }
    assert_raises { @scanner.luminance_of('#efg') }
    assert_raises { @scanner.luminance_of('#00000O') }
    assert_raises { @scanner.luminance_of('000000') }
    assert_raises { @scanner.luminance_of('#1234567') }
  end

  test 'should have a class method accessor' do
    assert_equal('#fff', ColorScanner.text_color('#000000'))
    assert_equal('#000', ColorScanner.text_color('#FFFFFF'))
  end
end
