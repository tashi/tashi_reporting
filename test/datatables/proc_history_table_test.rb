# frozen_string_literal: true

require_relative 'datatable_test_case'

class ProcHistoryTableTest < DatatableTestCase
  setup do
    create_result(Tashi::Result::PASSED, test_run: test_runs(:one))
    create_result(Tashi::Result::FAILED, test_run: test_runs(:two))
  end

  test 'first page no search' do
    table = table_json(dt_params)
    assert_dt_result(table, total: 2, filtered: 2)
    assert_table_data({ one: Tashi::Result::PASSED, two: Tashi::Result::FAILED }, table)
  end

  test 'first page search' do
    table = table_json(dt_params(sort_col: 'test_run_id', filter: 'Passed'))
    assert_dt_result(table, total: 2, filtered: 1)
    assert_table_data({ one: Tashi::Result::PASSED }, table)
  end

  test 'second page' do
    table = table_json(dt_params(sort_col: 'test_run_id', page_length: 1, start_page: 1))
    assert_dt_result(table, total: 2, filtered: 2)
    assert(1, table[:data].size)
  end

  test 'third page (empty)' do
    table = table_json(dt_params(sort_col: 'test_run_id', page_length: 1, start_page: 2))
    assert_dt_result(table, total: 2, filtered: 2)
    assert_empty(table[:data])
  end

  private

  def table_json(params)
    ProcHistoryTable.new(params, procedure: procedure.name).as_json
  end

  def assert_table_data(expectation, table)
    expectation.each do |run, result|
      row = table[:data].find { |data| data[:test_run_id] == test_runs(run).id }
      assert(row.present?)
      assert_equal(test_runs(run).specification, row[:specification])
      assert_equal(result, row[:result])
    end
  end
end
