# frozen_string_literal: true

require_relative 'datatable_test_case'

class ProceduresOverviewTableTest < DatatableTestCase
  test 'first page no search' do
    table = table_json(dt_params)
    assert_dt_result(table, total: 2, filtered: 2)
    assert_table_data(%i[first second], table)
  end

  test 'first page search' do
    table = table_json(dt_params(filter: procedures(:first).id))
    assert_dt_result(table, total: 2, filtered: 1)
    assert_table_data([:first], table)
  end

  test 'second page' do
    table = table_json(dt_params(page_length: 1, start_page: 1))
    assert_dt_result(table, total: 2, filtered: 2)
    assert(1, table[:data].size)
  end

  test 'third page (empty)' do
    table = table_json(dt_params(page_length: 1, start_page: 2))
    assert_dt_result(table, total: 2, filtered: 2)
    assert_empty(table[:data])
  end

  private

  def table_json(params)
    ProceduresOverviewTable.new(params).as_json
  end

  def assert_table_data(expectation, table)
    expectation.each do |run|
      row = table[:data].find { |data| data[:id] == procedures(run).id }
      assert(row.present?)
      assert_equal(procedures(run).description, row[:description])
      assert_equal(procedures(run).name, row[:name])
    end
  end
end
