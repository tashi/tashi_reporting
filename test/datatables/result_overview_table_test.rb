# frozen_string_literal: true

require_relative 'datatable_test_case'

class ResultOverviewTableTest < DatatableTestCase
  setup do
    create_result(Tashi::Result::PASSED, test_run: test_runs(:one))
    create_result(Tashi::Result::FAILED, test_run: test_runs(:two))
  end

  test 'first page no search' do
    table = table_json(dt_params)
    assert_dt_result(table, total: 2, filtered: 2)
    assert_table_data(%i[one two], table)
  end

  test 'first page search' do
    table = table_json(dt_params(filter: test_runs(:one).name))
    assert_dt_result(table, total: 2, filtered: 1)
    assert_table_data([:one], table)
  end

  test 'second page' do
    table = table_json(dt_params(page_length: 1, start_page: 1))
    assert_dt_result(table, total: 2, filtered: 2)
    assert(1, table[:data].size)
  end

  test 'third page (empty)' do
    table = table_json(dt_params(page_length: 1, start_page: 2))
    assert_dt_result(table, total: 2, filtered: 2)
    assert_empty(table[:data])
  end

  private

  def table_json(params)
    ResultOverviewTable.new(params).as_json
  end

  def assert_table_data(expectation, table)
    expectation.each do |run|
      row = table[:data].find { |data| data[:id] == test_runs(run).id }
      assert(row.present?)
      assert_equal(test_runs(run).specification, row[:specification])
      assert_equal(test_runs(run).name, row[:name])
    end
  end
end
