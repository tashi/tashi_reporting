# frozen_string_literal: true

require 'test_helper'

class DatatableTestCase < ActiveSupport::TestCase
  protected

  def assert_dt_result(table, total:, filtered:)
    assert_equal(total, table[:recordsTotal], 'total value does not match')
    assert_equal(filtered, table[:recordsFiltered], 'filtered value does not match')
  end
end
