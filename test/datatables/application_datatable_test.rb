# frozen_string_literal: true

require_relative 'datatable_test_case'

class ApplicationDatatableTest < DatatableTestCase
  class ApplicationDatatableTestImpl < ApplicationDatatable
    def data_wrapper
      data
    end

    def columns_wrapper
      columns
    end

    def model_class_wrapper
      model_class
    end
  end

  setup do
    @table = ApplicationDatatableTestImpl.new({})
  end

  test 'data not implemented' do
    assert_raises { @table.data_wrapper }
  end

  test 'columns not implemented' do
    assert_raises { @table.columns_wrapper }
  end

  test 'model_class not implemented' do
    assert_raises { @table.model_class_wrapper }
  end
end
