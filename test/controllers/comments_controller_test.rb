# frozen_string_literal: true

require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
    @result = create_result(Tashi::Result::PASSED)
    @result2 = create_result(Tashi::Result::FAILED)
  end

  test 'should create comment' do
    post '/comments', params: { text: 'Test Comment', result_id: @result.id }
    assert_response :success
    assert_equal(users(:one).name, json_response['user'])
    assert_equal(@result.id, json_response['procedure_result_id'])
    assert_equal('Test Comment', json_response['text'])
  end

  test 'should list comments for this result' do
    TestComment.create!(user: 'Foo', text: 'Test 1', procedure_result_id: @result.id)
    TestComment.create!(user: 'Bar', text: 'Test 2', procedure_result_id: @result.id)
    TestComment.create!(user: 'Baz', text: 'Test 3', procedure_result_id: @result2.id)

    get "/comments/#{@result.id}"
    assert_response :success
    assert(json_response.any? { |c| c['text'] == 'Test 1' })
    assert(json_response.any? { |c| c['text'] == 'Test 2' })
    assert(json_response.all? { |c| c['text'] != 'Test 3' })
    assert(json_response.all? { |c| c['procedure_result_id'] == @result.id })
  end

  test 'should list comments for all results' do
    TestComment.create!(user: 'Foo', text: 'Test 1', procedure_result_id: @result.id)
    TestComment.create!(user: 'Bar', text: 'Test 2', procedure_result_id: @result.id)
    TestComment.create!(user: 'Baz', text: 'Test 3', procedure_result_id: @result2.id)

    get "/comments/#{@result.id}/all"
    assert_response :success
    assert(json_response.any? { |c| c['text'] == 'Test 1' })
    assert(json_response.any? { |c| c['text'] == 'Test 2' })
    assert(json_response.any? { |c| c['text'] == 'Test 3' })
  end
end
