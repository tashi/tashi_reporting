# frozen_string_literal: true

require 'test_helper'

class GroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
  end

  test 'should get dependencies' do
    get "/groups/dependencies/#{test_runs(:one).id}"
    assert_response :success
    assert_equal(3, json_response.size)
    assert(json_response.all? { |entry| entry['test_run_id'] == test_runs(:one).id })
  end
end
