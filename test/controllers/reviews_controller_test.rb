# frozen_string_literal: true

require 'test_helper'

class ReviewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
  end

  test 'should get index' do
    get "/reviews/#{procedure.id}"
    assert_response :success
  end

  test 'should create review' do
    assert_difference('Review.count') do
      post '/reviews', params: { state: 'NEW', text: 'Test Review', procedure_id: procedure.id }
      assert_response :success
    end
  end
end
