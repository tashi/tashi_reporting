# frozen_string_literal: true

require 'test_helper'

class ReviewStatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
  end

  test 'should get index' do
    get '/review-states'
    assert_response :success
  end

  test 'should create state' do
    post '/review-states', params: { name: 'UnitTestState' }
    assert_response :redirect
  end

  test 'should not create state without name' do
    post '/review-states', params: { desc: 'no name' }
    assert_response :success
    assert flash[:error].include?("Name can't be blank")
  end

  test 'should not create state with same name twice' do
    post '/review-states', params: { name: 'Foo' }
    assert_response :redirect
    post '/review-states', params: { name: 'Foo' }
    assert_response :success
    assert flash[:error].include?('Name has already been taken')
  end
end
