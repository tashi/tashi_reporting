# frozen_string_literal: true

require 'test_helper'

class ProceduresControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
  end

  test 'should get index HTML' do
    get '/procedures'
    assert_response :success
  end

  test 'should get index JSON' do
    get '/procedures', params: dt_params
    assert_response :success
    assert_equal(%w[data recordsTotal recordsFiltered], json_response.keys)
  end

  test 'should get show by proc name' do
    get "/procedures/#{procedures(:first).name}"
    assert_response :success
  end

  test 'should get weekly_history' do
    get "/procedures/#{procedures(:first).id}/weekly_history"
    assert_response :success
    assert_equal(%w[passed observations failed not_exec], json_response.keys)
  end

  test 'should get total_results' do
    get "/procedures/#{procedures(:first).id}/total_results"
    assert_response :success
    assert_equal(%w[passed observations failed not_exec], json_response.keys)
  end

  test 'should get check_states' do
    get '/procedures/check_states'
    assert_response :success
    assert_equal(%w[msg], json_response.keys)
  end
end
