# frozen_string_literal: true

require 'test_helper'

class SuggestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
  end

  test 'should get tag suggestions' do
    get '/suggest/tags?q=Tag'
    assert_response :success
    assert_equal(%w[Tag1 Tag2], json_response)
  end

  test 'should get spr suggestions' do
    Spr.create!(reference: 'test')
    Spr.create!(reference: 'foo')
    get '/suggest/sprs?q=fo'
    assert_response :success
    assert_equal(%w[foo], json_response)
  end

  test 'should get procedures suggestions' do
    get '/suggest/procedures?q=fir'
    assert_response :success
    assert_equal(%w[FirstProcedure], json_response)
  end

  test 'should get requirements suggestions' do
    Requirement.create!(reference: 'test')
    Requirement.create!(reference: 'foo')
    get '/suggest/requirements?q=es'
    assert_response :success
    assert_equal(%w[test], json_response)
  end

  test 'should get requirements category suggestions' do
    RequirementCategory.create!(name: 'test')
    RequirementCategory.create!(name: 'foo')
    get '/suggest/req_category?q=es'
    assert_response :success
    assert_equal(%w[test], json_response)
  end

  test 'should get requirements group suggestions' do
    RequirementGroup.create!(name: 'bar')
    RequirementGroup.create!(name: 'foo')
    get '/suggest/req_group?q=b'
    assert_response :success
    assert_equal(%w[bar], json_response)
  end

  test 'should not fail on error input' do
    get '/suggest/not_implemented?q=b'
    assert_response :success
    assert_equal([], json_response)
  end
end
