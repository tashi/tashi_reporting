# frozen_string_literal: true

require 'test_helper'

class ApplicationControllerTest < ActiveSupport::TestCase
  class AppControllerTestImpl < ApplicationController
    def initialize
      super
      @_response ||= ActionDispatch::Response.new
    end

    # rubocop:disable Naming/MemoizedInstanceVariableName
    def request
      @_request ||= ActionDispatch::Request.empty
    end
    # rubocop:enable Naming/MemoizedInstanceVariableName

    def user=(user)
      @current_user = user
    end

    attr_reader :current_user

    def call(method)
      send(method)
    end
  end

  setup do
    @app = AppControllerTestImpl.new
  end

  test 'should deny admin if no user is set' do
    assert @app.call(:require_admin).include?('redirected')
  end

  test 'should deny tester if no user is set' do
    assert @app.call(:require_tester).include?('redirected')
  end

  test 'should deny witness if no user is set' do
    assert @app.call(:require_witness).include?('redirected')
  end

  test 'should deny user if no user is set' do
    assert @app.call(:require_user).include?('redirected')
  end

  test 'should deny aiv if no user is set' do
    assert @app.call(:require_aiv).include?('redirected')
  end

  test 'should allow user request if user is set' do
    assert_allowed(:user, :require_user)
  end

  test 'should allow admin request if user is admin' do
    assert_allowed(:admin, :require_admin)
  end

  test 'should allow tester request if user is tester' do
    assert_allowed(:tester, :require_tester)
  end

  private

  def assert_allowed(user, call)
    current_user = MiniTest::Mock.new
    current_user.expect :has?, true, [user]
    @app.user = current_user

    assert_nil @app.call(call)
  end
end
