# frozen_string_literal: true

require 'test_helper'

# rubocop:disable Metrics/ClassLength
class RequirementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
  end

  test 'should get index' do
    get '/requirements'
    assert_response :success

    get '/requirements', params: dt_params
    assert_response :success
  end

  test 'should get import view for ReqIF' do
    params = {
      file: Rack::Test::UploadedFile.new(resource_path('example.reqif')),
      type: 'ReqIF'
    }
    post '/requirements/import', params: params
    assert_response :success
  end

  test 'should get import view for CSV' do
    params = {
      file: Rack::Test::UploadedFile.new(resource_path('requirements.csv')),
      type: 'CSV'
    }
    post '/requirements/import', params: params
    assert_response :success
  end

  test 'should get import view for wrong input' do
    post '/requirements/import', params: { type: '´Something else' }
    assert_response :success
  end

  test 'should create requirement' do
    assert_difference('Requirement.count') do
      post '/requirements', params: { reference: 'UT-1', title: 'UnitTest', severity: 'NTH' }
    end
  end

  test 'should edit requirement' do
    requirement = Requirement.create!(reference: 'Original')
    post "/requirements/#{requirement.id}/edit",
         params: { reference: 'Changed', title: 'Title', description: 'Desc', severity: 'NTH' }
    assert_response :success
    requirement.reload
    assert_equal('Changed', requirement.reference)
    assert_equal('Title', requirement.title)
    assert_equal('Desc', requirement.description)
    assert_equal('NTH', requirement.severity)
  end

  test 'should not create requirement without reference' do
    post '/requirements', params: { title: 'UnitTest' }
    assert_response :bad_request
    assert response.body.include?("Reference can't be blank")
  end

  test 'should not create requirement with same reference twice' do
    post '/requirements', params: { reference: 'Foo' }
    post '/requirements', params: { reference: 'Foo' }
    assert_response :bad_request
    assert response.body.include?('Reference has already been taken')
  end

  test 'should link procedure and requirement' do
    requirement = Requirement.create!(reference: 'LinkMe')
    assert_difference('ProcedureRequirementsLink.count') do
      get "/requirements/link?dir=procToReq&proc=#{procedure.name}&req=#{requirement.reference}"
      assert_response :success
      assert_equal(procedure.to_json, response.body)
    end
  end

  test 'should link requirement and procedure' do
    requirement = Requirement.create!(reference: 'LinkMe')
    assert_difference('ProcedureRequirementsLink.count') do
      get "/requirements/link?dir=reqToProc&proc=#{procedure.name}&req=#{requirement.reference}"
      assert_response :success
      assert_equal(requirement.to_json, response.body)
    end
  end

  test 'should unlink requirement and proc' do
    link = ProcedureRequirementsLink.create!(requirement: Requirement.create!(reference: 'Test'), procedure: procedure)
    get "/requirements/unlink?req=#{link.requirement_id}&proc=#{link.procedure_id}"
    assert_response :success
    assert_nil ProcedureRequirementsLink.find_by(id: link.id)
  end

  test 'should not link procedure and requirement twice' do
    requirement = Requirement.create!(reference: 'LinkMe')
    get "/requirements/link?dir=procToReq&proc=#{procedure.name}&req=#{requirement.reference}"
    get "/requirements/link?dir=procToReq&proc=#{procedure.name}&req=#{requirement.reference}"
    assert_response :bad_request
    assert response.body.include?('Procedure and Requirement already linked')
  end

  test 'should get coverage matrix' do
    Requirement.create!(reference: 'Foo')
    get "/results/#{test_runs(:one).id}/req_coverage"
    assert_response :success
  end

  test 'should get coverage matrix JSON' do
    ProcedureRequirementsLink.create!(
      requirement: Requirement.create!(reference: 'Test1'),
      procedure: procedure
    )
    ProcedureRequirementsLink.create!(
      requirement: Requirement.create!(reference: 'Test2'),
      procedure: procedures(:first)
    )
    Requirement.create!(reference: 'Test3')
    create_result(Tashi::Result::PASSED)

    get "/results/#{test_runs(:one).id}/req_coverage", params: { format: :json }
    assert_response :success

    assert_equal(3, json_response.size)
    test1 = json_response.find { |data| data['req']['reference'] == 'Test1' }
    assert test1.present?
    assert_equal(1, test1['procedures'].size)
    assert_equal('Passed', test1['procedures'].first['result'])
    test2 = json_response.find { |data| data['req']['reference'] == 'Test2' }
    assert test2.present?
    assert_equal(1, test2['procedures'].size)
    assert_equal('N/A', test2['procedures'].first['result'])
    test3 = json_response.find { |data| data['req']['reference'] == 'Test3' }
    assert test3.present?
    assert_nil test3['procedures']
  end

  test 'should get req-matrix' do
    req = Requirement.create!(reference: 'Test_1')
    ProcedureRequirementsLink.create!(requirement: req, procedure: procedure)
    ProcedureRequirementsLink.create!(requirement: req, procedure: procedures(:first))
    ProcedureRequirementsLink.create!(requirement: Requirement.create!(reference: 'Test_2'), procedure: procedure)
    Requirement.create!(reference: 'Test_3')

    get '/requirements/matrix'
    assert_response :success

    assert_equal(3, json_response.size)
    test1 = json_response.find { |data| data['req']['reference'] == 'Test_1' }
    assert test1.present?
    assert_equal(2, test1['procedures'].size)
    test2 = json_response.find { |data| data['req']['reference'] == 'Test_2' }
    assert test2.present?
    assert_equal(1, test2['procedures'].size)
    test3 = json_response.find { |data| data['req']['reference'] == 'Test_3' }
    assert test3.present?
    assert_nil test3['procedures']
  end
end
# rubocop:enable Metrics/ClassLength
