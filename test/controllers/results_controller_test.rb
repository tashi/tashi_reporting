# frozen_string_literal: true

require 'test_helper'

class ResultsControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
    create_result(Tashi::Result::PASSED, test_run: test_runs(:one))
  end

  test 'should get index HTML' do
    get '/results'
    assert_response :success
  end

  test 'should get index JSON' do
    get '/results', params: dt_params
    assert_response :success
    assert_equal(%w[data recordsTotal recordsFiltered], json_response.keys)
  end

  test 'should get run details HTML' do
    get "/results/#{test_runs(:one).id}"
    assert_response :success
  end

  test 'should get run details JSON' do
    get "/results/#{test_runs(:one).id}", params: { format: :json }
    assert_response :success
    assert_equal(1, json_response.size)
    assert_equal(%w[group procedure tags start_time end_time css result comments summary], json_response[0].keys)
  end

  test 'should get run history' do
    get "/results/#{test_runs(:one).id}/history"
    assert_response :success
  end

  test 'should get procedure execution details' do
    get "/results/#{test_runs(:one).id}/#{procedure.name}"
    assert_response :success
  end

  test 'should get procedure execution details JSON' do
    get "/results/#{test_runs(:one).id}/#{procedure.name}", params: dt_params
    assert_response :success
    assert_equal(%w[data recordsTotal recordsFiltered], json_response.keys)
  end
end
