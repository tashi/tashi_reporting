# frozen_string_literal: true

require 'test_helper'

class StaticControllerTest < ActionDispatch::IntegrationTest
  test 'should get root' do
    get '/'
    assert_response :success
  end

  test 'should get about' do
    get '/about'
    assert_response :success
  end
end
