# frozen_string_literal: true

require 'test_helper'

class TagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
  end

  test 'should get index' do
    get '/tags'
    assert_response :success
  end

  test 'should create tag' do
    post '/tags', params: { name: 'Test', color: '#fff' }
    assert_response :redirect
  end

  test 'should delete tag' do
    tag = Tag.create!(name: 'DeleteMe', color: '#123')
    post '/tags/delete', params: { delid: tag.id }
    assert_response :redirect
    assert_nil Tag.find_by(id: tag.id)
  end

  test 'should create link' do
    post '/tags/delete', params: { object: 'procedure', object_id: procedure.id, tag: tags(:one).name }
    assert_response :redirect
  end

  test 'should remove link' do
    ProcedureTagsLink.create!(tag: tags(:one), procedure: procedure)
    get "/tags/unlink?tag=#{tags(:one).id}&procedure=#{procedure.id}"
    assert_response :success
    assert_empty ProcedureTagsLink.where(tag: tags(:one), procedure: procedure)
  end
end
