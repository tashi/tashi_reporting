# frozen_string_literal: true

require 'test_helper'

# rubocop:disable Metrics/ClassLength
class SprsControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
  end

  test 'should get import view' do
    post '/sprs/import', params: { file: Rack::Test::UploadedFile.new(resource_path('requirements.csv')) }
    assert_response :success
  end

  test 'should get import on illegal file' do
    post '/sprs/import', params: { file: 'foo' }
    assert_response :redirect
  end

  test 'should get index' do
    get '/sprs'
    assert_response :success

    get '/sprs', params: dt_params
    assert_response :success
  end

  test 'should get spr-matrix' do
    spr = Spr.create!(reference: 'Test_1')
    ProcedureSprLink.create!(spr: spr, procedure: procedure)
    ProcedureSprLink.create!(spr: spr, procedure: procedures(:first))
    ProcedureSprLink.create!(spr: Spr.create!(reference: 'Test_2'), procedure: procedure)
    Spr.create!(reference: 'Test_3')

    get '/sprs/matrix'
    assert_response :success

    assert_equal(3, json_response.size)
    test1 = json_response.find { |data| data['spr']['reference'] == 'Test_1' }
    assert test1.present?
    assert_equal(2, test1['procedures'].size)
    test2 = json_response.find { |data| data['spr']['reference'] == 'Test_2' }
    assert test2.present?
    assert_equal(1, test2['procedures'].size)
    test3 = json_response.find { |data| data['spr']['reference'] == 'Test_3' }
    assert test3.present?
    assert_nil test3['procedures']
  end

  test 'should get coverage matrix JSON' do
    ProcedureSprLink.create!(spr: Spr.create!(reference: 'Test1'), procedure: procedure)
    ProcedureSprLink.create!(spr: Spr.create!(reference: 'Test2'), procedure: procedures(:first))
    Spr.create!(reference: 'Test3')
    create_result(Tashi::Result::PASSED)

    get "/results/#{test_runs(:one).id}/spr_coverage", params: { format: :json }
    assert_response :success

    assert_equal(3, json_response.size)
    test1 = json_response.find { |data| data['spr']['reference'] == 'Test1' }
    assert test1.present?
    assert_equal(1, test1['procedures'].size)
    assert_equal('Passed', test1['procedures'].first['result'])
    test2 = json_response.find { |data| data['spr']['reference'] == 'Test2' }
    assert test2.present?
    assert_equal(1, test2['procedures'].size)
    assert_equal('N/A', test2['procedures'].first['result'])
    test3 = json_response.find { |data| data['spr']['reference'] == 'Test3' }
    assert test3.present?
    assert_nil test3['procedures']
  end

  test 'should get coverage matrix' do
    Spr.create!(reference: 'Foo')
    get "/results/#{test_runs(:one).id}/spr_coverage"
    assert_response :success
  end

  test 'should create spr' do
    assert_difference('Spr.count') do
      post '/sprs', params: { reference: 'UT-1', title: 'UnitTest', severity: 'NTH' }
    end
  end

  test 'should edit spr' do
    spr = Spr.create!(reference: 'Original')
    post "/sprs/#{spr.id}/edit", params: { reference: 'Changed', title: 'Title', description: 'Desc', severity: 'NTH' }
    assert_response :success
    spr.reload
    assert_equal('Changed', spr.reference)
    assert_equal('Title', spr.title)
    assert_equal('Desc', spr.description)
    assert_equal('NTH', spr.severity)
  end

  test 'should not create spr without reference' do
    post '/sprs', params: { title: 'UnitTest' }
    assert_response :bad_request
    assert response.body.include?("Reference can't be blank")
  end

  test 'should not create spr with same reference twice' do
    post '/sprs', params: { reference: 'Foo' }
    post '/sprs', params: { reference: 'Foo' }
    assert_response :bad_request
    assert response.body.include?('Reference has already been taken')
  end

  test 'should link procedure and spr' do
    spr = Spr.create!(reference: 'LinkMe')
    assert_difference('ProcedureSprLink.count') do
      get "/sprs/link?dir=procToSpr&proc=#{procedure.name}&spr=#{spr.reference}"
      assert_response :success
      assert_equal(procedure.to_json, response.body)
    end
  end

  test 'should link spr and procedure' do
    spr = Spr.create!(reference: 'LinkMe')
    assert_difference('ProcedureSprLink.count') do
      get "/sprs/link?dir=sprToProc&proc=#{procedure.name}&spr=#{spr.reference}"
      assert_response :success
      assert_equal(spr.to_json, response.body)
    end
  end

  test 'should unlink spr and proc' do
    link = ProcedureSprLink.create!(spr: Spr.create!(reference: 'Test'), procedure: procedure)
    get "/sprs/unlink?spr=#{link.spr_id}&proc=#{link.procedure_id}"
    assert_response :success
    assert_nil ProcedureSprLink.find_by(id: link.id)
  end

  test 'should not link procedure and spr twice' do
    spr = Spr.create!(reference: 'LinkMe')
    get "/sprs/link?dir=procToSpr&proc=#{procedure.name}&spr=#{spr.reference}"
    get "/sprs/link?dir=procToSpr&proc=#{procedure.name}&spr=#{spr.reference}"
    assert_response :bad_request
    assert response.body.include?('Procedure and SPR already linked')
  end
end
# rubocop:enable Metrics/ClassLength
