# frozen_string_literal: true

require 'test_helper'

class ProcedureResultExtensionTest < ActiveSupport::TestCase
  test 'find_by_run_and_name' do
    result = create_result(Tashi::Result::PASSED, test_run: test_runs(:one))

    assert_raises { ProcedureResult.find_by_run_and_name(test_runs(:one), 'Other') }
    assert_equal(result, ProcedureResult.find_by_run_and_name(test_runs(:one), procedure.name))
  end

  test 'result_css' do
    result = create_result('Not Executed')
    assert_equal('not_executed', result.result_css)
  end

  test 'summary_data' do
    result = create_result('Passed')
    result.message = '{:foo=>{:bar=>[], :baz=>[]}, :step=>{:failures=>[], :observations=>["got something"]}}'
    summary_data = result.summary_data
    assert_nil(summary_data['foo'])
    assert_equal({ 'failures' => [], 'observations' => ['got something'] }, summary_data['step'])
  end
end
