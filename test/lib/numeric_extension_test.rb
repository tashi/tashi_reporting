# frozen_string_literal: true

require 'test_helper'

class NumericExtensionTest < ActiveSupport::TestCase
  test 'percent_of' do
    assert_equal(10.0, 1.percent_of(10))
    assert_equal(200.0, 200.percent_of(100))
    assert_equal(60.0, 3.percent_of(5))
    assert_equal(31.58, 3.percent_of(9.5))
    assert_equal(35.5, (3.55).percent_of(10))
  end
end
