# frozen_string_literal: true

require 'test_helper'

class TagTest < ActiveSupport::TestCase
  test 'should validate color' do
    assert(Tag.create(name: 'Test1', color: '#abcdef').valid?)

    assert(!Tag.create(name: 'Test2', color: '#abcdeff').valid?)
    assert(!Tag.create(name: 'Test3', color: '#abcdeg').valid?)
    assert(!Tag.create(name: 'Test4', color: '#ab-').valid?)
  end

  test 'should determine matching text color' do
    assert_equal('#fff', Tag.create!(name: 'Test1', color: '#000').text_color)
    assert_equal('#fff', Tag.create!(name: 'Test2', color: '#F0F').text_color)
    assert_equal('#000', Tag.create!(name: 'Test3', color: '#0F0').text_color)
    assert_equal('#000', Tag.create!(name: 'Test4', color: '#FFF').text_color)
  end

  test 'should validate uniqueness of name' do
    assert(Tag.create(name: 'Test1', color: '#abcdef').valid?)
    assert(!Tag.create(name: 'Test1', color: '#abcdef').valid?)
  end

  test 'should link from procedure to tags' do
    ProcedureTagsLink.create!(tag: tags(:one), procedure: procedures(:first))
    ProcedureTagsLink.create!(tag: tags(:two), procedure: procedures(:first))

    tags = Procedure.find_by(id: procedures(:first).id).tags
    assert_equal(2, tags.size)
    assert(tags.include?(tags(:one)))
    assert(tags.include?(tags(:two)))
  end

  test 'should translate into json' do
    tag = Tag.create!(name: 'Foo', desc: 'Bar', color: '#000')
    assert_equal({ 'name' => 'Foo', 'desc' => 'Bar', 'color' => '#000', 'text_color' => '#fff' },
                 tag.as_json.reject { |k, _v| k == 'id' })
  end
end
