# frozen_string_literal: true

require 'test_helper'

class ProcedureHistoryTest < ActiveSupport::TestCase
  test 'weekly_stats summary all values' do
    2.times { create_result(Tashi::Result::PASSED) }
    3.times { create_result(Tashi::Result::OBSERVATIONS) }
    4.times { create_result(Tashi::Result::FAILED) }
    5.times { create_result(Tashi::Result::NOT_EXECUTED) }

    result = ProcedureHistory.find_by!(id: procedure.id).weekly_stats

    assert_equal(2, result[:passed].values.last)
    assert_equal(3, result[:observations].values.last)
    assert_equal(4, result[:failed].values.last)
    assert_equal(5, result[:not_exec].values.last)
  end

  test 'weekly_stats summary multiple weeks' do
    create_result(Tashi::Result::PASSED)
    2.times { create_result(Tashi::Result::PASSED, start_time: 1.week.ago, end_time: 1.week.ago) }
    3.times { create_result(Tashi::Result::PASSED, start_time: 2.weeks.ago, end_time: 2.weeks.ago) }
    4.times { create_result(Tashi::Result::PASSED, start_time: 3.weeks.ago, end_time: 3.weeks.ago) }
    5.times { create_result(Tashi::Result::PASSED, start_time: 4.weeks.ago, end_time: 4.weeks.ago) }

    result = ProcedureHistory.find_by!(id: procedure.id).weekly_stats

    assert_equal(4, result[:passed].keys.size)
    assert_equal([4, 3, 2, 1].sort, result[:passed].values.sort, 'The result 4 weeks ago (5) should not be included')
  end

  test 'total_results summary' do
    5.times { create_result(Tashi::Result::PASSED) }
    4.times { create_result(Tashi::Result::OBSERVATIONS) }
    3.times { create_result(Tashi::Result::FAILED) }
    2.times { create_result(Tashi::Result::NOT_EXECUTED) }

    result = ProcedureHistory.find_by!(id: procedure.id).total_results

    assert_equal(5, result[:passed])
    assert_equal(4, result[:observations])
    assert_equal(3, result[:failed])
    assert_equal(2, result[:not_exec])
  end
end
