# frozen_string_literal: true

require 'test_helper'

class ReportingTestRunTest < ActiveSupport::TestCase
  setup do
    create_result(Tashi::Result::PASSED)
    create_result(Tashi::Result::PASSED, procedure: procedures(:first))
    create_result(Tashi::Result::FAILED, procedure: procedures(:second))
  end

  test 'json result summary' do
    json = ReportingTestRun.find_by!(id: test_runs(:one)).result_json
    assert_equal(2, json.select { |r| r[:result] == Tashi::Result::PASSED }.size)
    assert_equal(1, json.select { |r| r[:result] == Tashi::Result::FAILED }.size)
    assert_equal(1, json.select { |r| r[:procedure] == procedure.name }.size)
  end
end
