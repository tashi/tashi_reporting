# frozen_string_literal: true

require 'test_helper'

class ImporterRequirementsTest < ActiveSupport::TestCase
  require 'csv'

  test 'should import csv from form params' do
    @importer = Importer::Requirements.new(import_params)
    assert_equal({ imported: 3, skipped: 0 }, @importer.import)

    assert Requirement.exists?(reference: 'imp-1')
    assert Requirement.exists?(reference: 'imp-2')
    assert Requirement.exists?(reference: 'imp-3')
  end

  private

  def import_params
    {
      collision_handling: :override,
      'category' => :category,
      'group' => :group,
      'reference' => :reference,
      csv: req_csv_json
    }
  end

  def req_csv_json
    CSV.parse(File.read(resource_path('requirements.csv')), headers: true, col_sep: ';').to_json
  end
end
