# frozen_string_literal: true

require 'test_helper'

class ReqifReaderTest < ActiveSupport::TestCase
  setup do
    @req_if = ReqIf::Reader.from_xml(resource_path('example.reqif'))
  end

  test 'should read data-types' do
    assert_equal({ type: :string, name: 'T_String32k' }, @req_if.data_types['rmf-fa6300d0-9e8b-4b57-a6ff-10a6bc76e604'])
    assert_equal({ type: :string, name: 'T_String32' }, @req_if.data_types['rmf-1d2c140f-97e9-4d11-9554-63b2bee2dc3c'])

    assert_equal(:enum, @req_if.data_types['rmf-cf80ba48-e543-46d8-99b0-acbdc6541dd9'][:type])
    severities = @req_if.data_types['rmf-4a32761e-e015-4696-9578-0be539234b9f'][:values]
    assert_equal('HIGH', severities['rmf-2c569fa8-459e-432e-b2a9-26ae99e0c2d1'])
    assert_equal('MEDIUM', severities['rmf-2c569fa8-459e-432e-b2a9-26ae99e0c2dc'])
    assert_equal('LOW', severities['rmf-2c569fa8-459e-432e-b2a9-26ae99e0c2d3'])
  end

  test 'should read object-types' do
    assert_equal('Requirement Type', @req_if.object_types['rmf-e7aa3d46-6556-45bd-969b-d7534fa97ad1'][:name])
    assert_equal('Headline Type', @req_if.object_types['rmf-3e0b6fad-930b-4d9e-92fd-b84619282414'][:name])
    assert_equal(%w[Title ID],
                 @req_if.object_types['rmf-3e0b6fad-930b-4d9e-92fd-b84619282414'][:columns].values.map { |c| c[:name] })
  end

  test 'should read spec-objects' do
    object = @req_if.spec_objects['rmf-3abb1510-285c-4e64-a921-d2379c18dae8']
    assert_equal('rmf-e7aa3d46-6556-45bd-969b-d7534fa97ad1', object[:spec_type])
    assert_equal(['', 'Execution Environment Headless Eclipse', '', 'Mark Brörkens', 'sws_6'].sort,
                 object[:values].map { |e| e[:value] }.sort)

    headline = @req_if.spec_objects['rmf-98d872e4-145b-4520-8430-f51ff773b6a4']
    assert_equal('rmf-3e0b6fad-930b-4d9e-92fd-b84619282414', headline[:spec_type])
    assert_equal(['Naming conventions and Definitions', 'sws_0001'].sort,
                 headline[:values].map { |e| e[:value] }.sort)
  end

  test 'should read spec-types' do
    spec = @req_if.spec_types['rmf-b37ec177-5ebb-4b78-8626-b453cf6e1784']
    assert_equal('Specification Type', spec[:name])
    assert_equal(1, spec[:columns].size)
    assert_equal('Description', spec[:columns].values.first[:name])
  end

  test 'should read specifications' do
    assert_equal(1, @req_if.specifications.size)
    spec = @req_if.specifications.values.first
    assert_equal('Specification Document', spec[:name])
    assert_equal('Software Requirements Specification', spec[:columns].first[:value])

    first_level_children = %w[rmf-98d872e4-145b-4520-8430-f51ff773b6a4 rmf-226476e2-82e5-462b-9955-631e82efe981
                              rmf-266b5236-5142-4459-9f9e-f3e5becf1fdc rmf-0ea65654-db66-4b72-a8fb-4fc8fcb6126b
                              rmf-1a4974f1-89e3-4c38-8694-fa2e846a3fd6 rmf-d0dabe58-8ef8-4403-b979-fd86192ebd10
                              rmf-dc2e5fae-12a1-43ae-be30-73a97febac1f rmf-da90477e-d906-4d3f-bc2e-c727464bda40
                              rmf-bf80257b-429a-4425-9212-161391b150fc rmf-c6a2b4ce-d751-4079-ad03-ffa89f94241d
                              rmf-ebfd53fd-c581-4eba-935d-4450d9f8925d rmf-2c3d9b2b-70c7-47a4-8e7e-97391904f0b1]
    assert_equal(first_level_children, spec[:children].values.map { |c| c[:ref] })
  end
end
