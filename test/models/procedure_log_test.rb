# frozen_string_literal: true

require 'test_helper'

class ProcedureLogTest < ActiveSupport::TestCase
  setup do
    @result = create_result(Tashi::Result::PASSED)
  end

  test 'no log' do
    with_tempdir do
      log = ProcedureLog.parse(@result)
      assert_not(log.logfile_present?)
      assert_empty(log.steps)
      assert_empty(log.files)
      assert_equal(@result.id, log.id)
    end
  end

  test 'list files' do
    with_tempdir do |dir|
      create_logfile(target: dir)
      create_sample_files(target: create_working_dir(dir))
      files = ProcedureLog.find_files(@result)

      assert(files.find { |f| f[:name].to_s == 'lorem.txt' })
      assert(files.find { |f| f[:name].to_s == 'Procedure Execution Logfile (json)' })

      subfolder = files.find { |f| f[:name].to_s == 'subfolder' }
      assert(subfolder.present?)
      assert(subfolder[:children].find { |f| f[:name].to_s == 'ipsum.txt' })
    end
  end

  test 'parse json log' do
    with_tempdir do |dir|
      create_logfile(target: dir, resource: 'example_result_log.json')
      create_working_dir(dir)

      log = ProcedureLog.parse(@result)

      assert(log.logfile_present?)
      assert_equal(3, log.steps.size)
      assert_equal(%i[duration expectation instruction log observation result title], log.steps[1].keys.sort)
    end
  end

  test 'parse xml log' do
    with_tempdir do |dir|
      create_logfile(target: dir, format: :xml, resource: 'example_result_log.xml')
      create_working_dir(dir)

      log = ProcedureLog.parse(@result)

      assert(log.logfile_present?)
      assert_equal(3, log.steps.size)
      assert_equal(%i[duration expectation instruction log observation result title], log.steps[1].keys.sort)
    end
  end

  private

  def create_logfile(target:, format: :json, resource: nil)
    logfile = File.join(target, "#{procedure.name.parameterize}.#{format}")
    if resource.blank?
      File.write(logfile, '{}') if format == :json
      File.write(logfile, '') if format == :xml
    else
      FileUtils.cp(resource_path(resource), logfile)
    end
    logfile
  end

  def create_sample_files(target:)
    subfolder = File.join(target, 'subfolder')
    FileUtils.mkdir_p(subfolder)
    File.write(File.join(target, 'lorem.txt'), 'Lorem')
    File.write(File.join(subfolder, 'ipsum.txt'), 'Ipsum')
  end

  def with_tempdir
    Dir.mktmpdir('list_files') do |dir|
      @result.test_run.stub(:results_dir, dir) do
        yield dir
      end
    end
  end

  def create_working_dir(dir)
    working_dir = File.join(dir, procedure.name.parameterize)
    FileUtils.mkdir_p(working_dir)
    working_dir
  end
end
