# frozen_string_literal: true

require 'test_helper'

class ReqifImporterTest < ActiveSupport::TestCase
  test 'should import reqif from form params' do
    @importer = ReqIf::Importer.new(import_params)
    assert_equal({ imported: 2, skipped: 0 }, @importer.import)

    assert Requirement.exists?(reference: 'Req1')
    assert Requirement.exists?(reference: 'Req2')
  end

  private

  def import_params
    {
      collision_handling: :override,
      '0::-::category' => :category,
      '0::-::group' => :group,
      '1::-::reference' => :reference,
      reqif: reqif_json
    }
  end

  # rubocop:disable Metrics/MethodLength
  def reqif_json
    [
      {
        level: 0,
        fields: [{ ref: 'category', value: 'Cat1' }, { ref: 'group', value: 'Group1' }, { ref: 'foo', value: 'bar' }],
        children: [
          {
            level: 1,
            fields: [{ ref: 'reference', value: 'Req1' }, { ref: 'foo', value: 'bar' }],
            children: []
          },
          {
            level: 1,
            fields: [{ ref: 'reference', value: 'Req2' }, { ref: 'foo', value: 'bar' }],
            children: []
          }
        ]
      }
    ].to_json
  end
  # rubocop:enable Metrics/MethodLength
end
