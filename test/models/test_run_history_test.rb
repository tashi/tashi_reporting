# frozen_string_literal: true

require 'test_helper'

class TestRunHistoryTest < ActiveSupport::TestCase
  setup do
    @result = create_result(Tashi::Result::PASSED)
    @history = TestRunHistory.of(test_runs(:two).id)
  end

  test 'should have information on current run' do
    assert_equal(test_runs(:two).id, @history.current.id)
    assert_equal(test_runs(:two).id, @history.run_id)
  end

  test 'should provide ids of all reported runs' do
    assert_equal([test_runs(:two).id, test_runs(:one).id], @history.data[:run_ids])
  end

  test 'should provide summary of all reported runs' do
    assert_equal([{
                   :id => 2,
                   :name => 'Unit Test Run 2',
                   :duration => 1440.0,
                   :total => 0,
                   'Not Executed' => 0,
                   'Passed' => 0,
                   'Passed with observations' => 0,
                   'Failed' => 0
                 }, {
                   :id => 1,
                   :name => 'Unit Test Run 1',
                   :duration => 60.0,
                   :total => 1,
                   'Not Executed' => 0,
                   'Passed' => 1,
                   'Passed with observations' => 0,
                   'Failed' => 0
                 }], @history.data[:runs])
  end

  test 'should provide results of all reported procedures' do
    assert_equal([{
                   procedure: { id: @result.procedure.id, name: 'ProcForTesting', tags: [] },
                   executions: [{ run: 1, css: 'cell-passed', result: 'Passed', summary: [] }]
                 }], @history.data[:results])
  end
end
