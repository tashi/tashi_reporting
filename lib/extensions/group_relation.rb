# frozen_string_literal: true

##
# Extension of the Tashi GroupRelation model.
#
# Need to override it in extensions as we can ensure the class is reopened and overwritten
# in this case. Add all methods that should be available when using the foo.group_relations
# accessor as provided by active record.
#
# Author johannes.hoelken@hawk.de
class GroupRelation
  # improve json creation with child and parent data
  def as_json(_options = {})
    { test_run_id: test_run_id, parent: parent.as_json, child: child.as_json }
  end
end
