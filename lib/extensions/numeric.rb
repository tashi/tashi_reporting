# frozen_string_literal: true

# Extension of numerical classes
# Author johannes.hoelken@hawk.de
class Numeric
  # Calculate the percentage of current value in relation to a given +total+.
  #    1.percent_of(10)    # =>  10.0
  #    200.percent_of(100) # => 200.0
  #    3.percent_of(5)     # =>  60.0
  # @param total [Numeric] the total value
  def percent_of(total)
    (to_f / total * 100.0).round(2)
  end
end
