# frozen_string_literal: true

##
# Extension of the Tashi Procedure model.
#
# Need to override it in extensions as we can ensure the class is reopened and overwritten
# in this case. Add all methods that should be available when using the foo.procedures
# accessor as provided by active record.
#
# Author johannes.hoelken@hawk.de
class Procedure
  has_many :procedure_tags_links
  has_many :tags, through: :procedure_tags_links

  has_many :procedure_spr_links
  has_many :sprs, through: :procedure_spr_links

  has_many :procedure_requirements_links
  has_many :requirements, through: :procedure_requirements_links

  has_many :reviews
  has_one :last_review, -> { order(created_at: :desc).limit(1) }, class_name: 'Review'
  has_one :review_state, through: :last_review, class_name: 'ReviewState'

  def current_state
    return review_state if review_state.present?

    ReviewState.new(name: 'UNKNOWN', description: 'No review state selected.')
  end

  def spr_names
    sprs.select(:reference).order(:reference).map(&:reference)
  end

  def requirement_names
    requirements.select(:reference).order(:reference).map(&:reference)
  end
end
