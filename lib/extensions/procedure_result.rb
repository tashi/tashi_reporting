# frozen_string_literal: true

##
# Extension of the Tashi ProcedureResult model.
#
# Need to override it in extensions as we can ensure the class is reopened and overwritten
# in this case. Add all methods that should be available when using the foo.procedure_results
# accessor as provided by active record.
#
# Author johannes.hoelken@hawk.de
class ProcedureResult
  has_many :test_comments

  alias comments test_comments

  # Find the procedure result by the id of the run and the name of the procedure
  # supports speaking urls and breadcrumbs
  # @param (Integer) run_id the db id of the test run
  # @param (String) proc_name the name of the procedure
  # @return (ProcedureResult) the result of the procedure within the targeted run or nil if none is found
  def self.find_by_run_and_name(run_id, proc_name)
    result = ProcedureResult.joins(:procedure, :test_run)
                            .where('procedures.name = ? AND test_run_id = ?', proc_name, run_id).first
    raise "Could not resolve procedure #{proc_name} in run ##{run_id}." if result.blank?

    result
  end

  # collect only non-empty entries from the messages JSON string.
  # @return (Hash) {step: {failures:Array, observations:Array}}
  def summary_data
    return [] if message.blank?

    summary = JSON.parse(message.gsub(/:(\w*)\s?=>/) { "\"#{Regexp.last_match[1]}\":" })
    summary.reject! { |_k, v| v.values.all?(&:blank?) }
    summary
  end

  # convert the result value to a css-class string format.
  # @return (String)
  def result_css
    result.downcase.gsub(' ', '_')
  end
end
